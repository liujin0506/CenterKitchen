var remoteAppId_NTGJ = window.location.hash.replace(/^#\/|\/$/g, '');
define([
    "/assets/libraries/sortable/sortable.js",
    "/assets/module/gallery/common/js/filemanage.js",
    "/assets/module/gallery/common/js/uploadfiles.js",
], function (Sortable) {

    cloudModule.controllerProvider.register('collect-admin-galleryadd-controller', 
    ['$scope', 'Modal', '$http', '$timeout', '$location','UploadFileService','commonService',
      function($scope, Modal, $http, $timeout, $location,UploadFileService,commonService) {
        $scope.content = $scope.editContent.content;
        var draftModal, submitSuccessModal, submitFailedModal, draftFailedModal;
        //记录摘要和关键词是否被手动输入，bool值
        var descripttionState = true,
            keywordsState = true,
            titleChanged, contentChanged, contentBak = '',
            titleBak = '',
            deltaWord = 5;

        var descriptionDom = $('textarea.description');
        var submitBtn = $('.submit');
        var scroll1, scroll2;
        var query = $location.search();
        var api = {
            submitUrl: '/gallery/index/add',
            autoabstractUrl: '/article/index/prenlp',
            getContent: query.isDraft == '1' ? '/gallery/index/dcontent' : '/gallery/index/Content',
            restatus:'app/collect/Content/restatus'
        };
        var mode = 0; //模式: 0-正常，1-草稿，2-编辑
        var dataBak = {};
        var loadimgTimer;

        var Validator = function() {
            var self = this;
            this.errs = {
                _length: 0
            };
            this.tips = '';
            this.defers = [];
            this.regular = {};
            this.data = {};
            this.decorators = {
                minLength: function(data, len) {
                    if(!data){
                        return false;
                    }
                    return data.length >= len;
                },
                maxLength: function(data, len) {
                    data = data || '';
                    return data.constructor === String ? (Math.ceil(data.replace(/[\u1000-\uffff]{1}/g, 'aa').length / 2) <= len) : false;
                }
            };

            //表单项注册规则
            this.register = function(name, reg, tips) {
                this.regular[name] = this.regular[name] || [];
                this.regular[name].push({
                    reg: reg,
                    tips: tips,
                    name: name
                });

                return this;
            };

            this.start = function() {
                var regular = this.regular;
                var decorators = this.decorators;
                var self = this;
                self.errs = {
                    _length: 0
                };
                angular.forEach(regular, function(regs, name) {
                    var value = self.data[name];
                    //验证规则的集合
                    angular.forEach(regs, function(eachReg) {
                        //单个验证规则是Object类型
                        angular.forEach(eachReg.reg, function(val, regname) {
                            if (!decorators[regname](value, val)) {
                                self.errs[name] = self.errs[name] || [];
                                self.errs[name].push({
                                    name: name,
                                    errType: regname,
                                    tips: eachReg.tips
                                });
                                self.errs._length++;
                            }
                        });
                    });
                });
            };
        };
        var uploader = new UploadFileService();
        var validator = new Validator();

        validator.register('title', {
            maxLength: 36
        }, '标题不能超过36字');
        validator.register('title', {
            minLength: 1
        }, '标题不能为空');
        validator.register('content', {
            minLength: 3
        }, '内容不能为空');
        validator.register('description', {
            minLength: 1,
            maxLength: 128
        }, '摘要不合要求');
        validator.register('keyword', {
            minLength: 1
        }, '关键词不能为空');
        validator.register('channel', {
            minLength: 1
        }, '未选择渠道');

        function switchViewsData(mode) {
            if (mode === 1) {
                //审核
                $scope.view.publishBtn = '发稿';
                $scope.view.successModalTitle = '已发稿';
                $scope.view.successModalContent = '已进入待审，需要审核通过后才会发布';
                $scope.view.failModalTitle = '发稿失败';
                $scope.view.disablePreview = true;
                $scope.view.enablePriority = 1;
            } else {
                //不审核
                $scope.view.publishBtn = '发布';
                $scope.view.successModalTitle = '发布成功';
                $scope.view.failModalTitle = '发布失败';
                $scope.view.successModalContent = ' ';
                $scope.view.disablePreview = false;
                $scope.view.enablePriority = $scope.view.enableAuditCheckbox ? 2 : 0; //0表示隐藏，2表示禁用，1表示开启
            }
        }

        function resetFileInput(dom) {
            $(dom).val('');
            $(dom).each(function(val, i) {
                var prev = $(val).prev();
                var newdom = val.outterHTML;

                $(val).remove();
                prev.after(newdom);
            });
        }

        function renderImageList() {
            var container = $('.gallery-preview-childpage .attachment-display-panel-inner');
            var cols = ~~((container.width() + 30) / 258); //向下取整
            var styleStr = '<style>._imagelist .item{margin-left: 30px!important;margin-bottom:30px!important;}._imagelist .item:nth-child(' + cols + 'n+2){margin-left: 0!important;}</style>';
            $.each(container.find('.item'), function(i, val) {
                val.style.position = 'static';
            });
            if (container.next().length) {
                container.next().remove();
                // container.next().html(styleStr);
            }

            container.after(styleStr);

        }

        // 更新用于视图显示的图片集合,cacheId用于清除ngRepeat缓存
        function updateImgInfo(localId,serverUrl){
            ($scope.data.imgs || []).forEach(function(img){
                if(img.localId === localId){
                    img.serverUrl = serverUrl;
                    img.cacheId = Math.random();
                }
            });
            ($scope.data.lastImgs || []).forEach(function(img){
                if(img.localId === localId){
                    img.serverUrl = serverUrl;
                    img.cacheId = Math.random();
                }
            });

            setTimeout(function() {
                $scope.$apply();
            }, 0);
        }

        /*提取缩略图*/
        function extractThumb() {
            var filedata = uploader.getFile(uploader.getCompletedFiles()[0]);
            if(!filedata){return;}
            $scope.thumb.id = filedata.serverId;
            $scope.thumb.url = filedata.url;
            setTimeout(function() {
                $scope.$apply();
            }, 0);
        }

        cloud.selectedMenu = [];
        $scope.titleNum = 36;
        $scope.isNewContent = true;
        $scope.description = '';
        $scope.submitValid = false;
        $scope.thumb = {};
        $scope.publishState = 'unpublish';
        $scope.chooseThumbFromLibrary = false;
        $scope.view = {
            enablePriority: 0
        }; //界面显示文字
        $scope.data = {};
        $scope.imageList = [];

        // ----------------- 云稿库检测 ------------ //

        $scope.wordCount = function(str) {
            return str ? Math.ceil(str.replace(/[\u1000-\uffff]{1}/g, 'aa').length / 2) : 0;
        };
        $scope.previewImage = function(localId) {
            $scope.current = $scope.data.imgs.filter(function(val) {
                return val.localId === localId;
            })[0];
        };
        $scope.switchMode = function(newVal){
            $scope.data.enableAudit = newVal;
            switchViewsData($scope.data.enableAudit ? 1 : 0);
        };

        /* 发布，保存草稿 */
        submitSuccessModal = function() {
            var buttons = [{
                name: '返回',
                event: function() {
                    var parentScope = window.parent.angular.element($(window.parent.document).find('[ng-controller]')[1]).scope();
                    parentScope.$parent.tpl = $scope.template.normal;
                }
            }];
            if (!$scope.view.disablePreview) {
                buttons.unshift({
                    name: $scope.view.disablePreview ? '' : '查看',
                    cmd: 'view',
                    event: function() {
                        return true;
                    }
                });
            }
            return new Modal({
                type: 'success',
                title: $scope.view.successModalTitle,
                content: $scope.view.successModalContent,
                buttons: buttons
            });
        };
        submitFailedModal = new Modal({
            type: 'error',
            title: $scope.view.failModalTitle,
            content: ''
        });
        draftFailedModal = new Modal({
            type: 'error',
            title: '存入草稿失败',
            content: ''
        });
        draftModal = new Modal({
            type: 'alert',
            title: ' ',
            content: ' ',
            buttons: [{
                name: "取消",
                cmd: "canel"
            }, {
                name: "不保存",
                cmd: "delete",
                event: function() {
                    window.parent.cloud.transformOut();
                }
            }, {
                name: "保存",
                event: function() {
                    $scope.submit(true);
                }
            }]
        });

        $scope.submit = function(isdraft) {
            var data, channels = [],
                category = [],
                errors = [];
            if ($scope.onpublish) {
                return;
            }
            if (!isdraft) {
                if (!submitBtn.hasClass('active')) {
                    return;
                }
            }
            isdraft = isdraft || false;
            $('.warns').removeClass('warns');


            data = collectData();
            data.recommend = data.recommend ? 'true' : 'false';

            // 判断id的类型，分为草稿id和contentid
            if(query.isDraft == '1' && query.id){
                data.draftid = query.id;
            }else{
                if ($scope.id) {
                    data.id = $scope.id;
                }
            }

            if (isdraft) {
                data.isdraft = 1;
            } else {
                //验证表单数据
                validator.data = data;
                validator.start();
                if ('title' in validator.errs) {
                    submitFailedModal.data.title = validator.errs['title'][0].tips;
                    submitFailedModal.open();
                    return;
                }
                if ('content' in validator.errs) {
                    submitFailedModal.data.title = validator.errs['content'][0].tips;
                    submitFailedModal.open();
                    return;
                }
                $scope.showWarning = true;

                if (validator.errs._length || !($scope.category||[]).length) {
                    if ('description' in validator.errs) {
                        // 摘要不能为空或超字数
                        $('.sider-container .description').addClass('warns');
                    }
                    if ('keyword' in validator.errs) {
                        // 关键词不能为空
                        $('.sider-container .keyword .ui-taginput').addClass('warns');
                    }
                    if ('channel' in validator.errs) {
                        $('.sider-container .channel').addClass('warns');
                    }
                    return false;
                }
            }

            $scope.onpublish = true;
            if (uploader.getUncompletedFiles.length) {
                uploader.onAllComplete = function() {
                    $scope.submit(isdraft);
                };
            } else {
                uploader.onAllComplete = function() {};
            }

            $.post(api.submitUrl, data).success(function(res) {
                $scope.onpublish = false;
                if (isdraft) {
                    if (res.state) {
                        window.parent.cloud.transformOut();
                        setTimeout(function() {
                            $('[ng-controller="collect-admin-galleryadd-controller"]').trigger('init');
                        }, 1);
                    } else {
                        draftFailedModal.data.title = res.error;
                        draftFailedModal.open();
                    }
                    return;
                }
                if (res.state) {
                    data.contentid = $scope.contentid;
                    data.siteid = cloud.siteId;
                    data.status = 6;
                    $http.post('app/collect/Content/recontent', data).success(function(res) {});
                    //$http.get(api.restatus+'?'+'siteid='+cloud.siteId+'&contentid='+$scope.contentid+'&status=6');
                    // 云稿库记录log
                    if (parent && parent.cloud.superContent) {
                        $http.get('/supercontent/index/releaseHandle', {params: {
                            siteid: cloud.siteId,
                            customid: cloud.custom.id,
                            info_title: $scope.title,
                            info_appid: 1,
                            info_contentid: parent.cloud.superContent.id,
                            info_siteid: parent.cloud.superContent.siteid,
                            contentid: res.id,
                            superid: parent.cloud.superContent.superid
                        }});
                    }
                    $scope.$apply(function() {
                        $scope.publishState = 'published';
                    });
                    submitSuccessModal().open().then(function(cmd) {
                        if (cmd === 'view') {
                            window.open(res.url, "_blank");
                        }
                        // ------------------------- /
                        // 云稿库iframe调用检测
                        if (parent && parent.cloud.superContent) {
                            parent.location.reload();
                            return;
                        }
                        // ------------------------- /
                        //window.parent.cloud.transformOut(data.enableAudit ? null : res.id);
                        $scope.editContent.state = false;
                    });
                } else {
                    submitFailedModal.data.title = res.error;
                    submitFailedModal.open();
                }
            });
        };


        $scope.openThumbDialog = function() {
            if (!$scope.aca('attachment/thumblist', 'system')) {
                return Modal.alert('无此权限');
            }
            $(this).transform({
                reload: false,
                effect: 'fadeIn',
                url: '#/system/attachment/thumb?id=' + ($scope.thumb.id || '')
            });
            cloud.transformOut = function() {
                $(this).transform({
                    action: 'out'
                });
            };
        };
        //保存草稿
        /* 返回 */
        var backModal = new Modal({
            type: 'alert',
            title: ' ',
            content: ' ',
            buttons: [{
                name: "取消",
                cmd: false
            }, {
                name: "不保存",
                cmd: true
                }
            ]
        });
        $scope.goback = function() {
            var temp, tipstitle;
                if ($scope.title && $scope.title.length > 20) {
                    tipstitle = $scope.title.substr(0, 20) + '...';
                } else {
                    tipstitle = $scope.title;
                }
                backModal.data.title = '内容尚未保存，您确定要退出?';
                backModal.open().then(function(res){
                  if(res){
                    $scope.editContent.state = false;
                  }
                });
        };
        $scope.revoke = function() {
            $scope.displayPreviewPanel = false;
            uploader.revoke();
            $scope.data.imgs = uploader.getFilesUrl();
            $scope.data.lastImgs = [];
            if($scope.data.imgs.length === 0){
                $scope.thumb = {};
                $scope.thumb.url = '';
            }
            if (!uploader.getFile($scope.currentImage)) {
                $scope.currentImage = undefined;
                $scope.current = undefined;
            }
            resetFileInput($('input[type=file]')); //清空之前选择的文件
        };
        $scope.pushFiles = function(files, e) {
            if (!files || files.constructor === String && files.indexOf('.') < 0) {
                return;
            }
            if (files.constructor === FileList) {
                var  form = document.createElement('input');
                form.setAttribute('type','file');
                form.setAttribute('multiple',true);
                form.setAttribute('accept','image/*');
                $(form).insertBefore($(e.target)).css({
                    position: 'absolute',
                    left: '-10000px',
                    zIndex: -1,
                    visibility: 'hidden'
                }).on('change',function(e){
                    $scope.pushFiles(e.originalEvent.target.files,e);
                });
                $(e.target).remove();
            }

            if (files.length > 1 && files.constructor !== String) {
                $scope.displayPreviewPanel = true;
            }
            uploader.ready(function(filesId) {
                //插入当前选择的文件后面
                if ($scope.currentImage !== undefined) {
                    var oldsort = uploader.getCurrentSort();
                    var index;
                    filesId.forEach(function(item) {
                        var i = oldsort.indexOf(item);
                        if(i>-1){
                            oldsort.splice(i, 1);
                        }
                    });
                    index = oldsort.indexOf($scope.currentImage);
                    if (index > -1) {
                        var back = oldsort.splice(index + 1);
                        oldsort = oldsort.concat(filesId.concat(back));
                        uploader.updateSort(oldsort);
                    }
                }
                $scope.data.imgs = uploader.getFilesUrl();
                $scope.data.lastImgs = $scope.displayPreviewPanel ? uploader.getLastFiles() : [];
                $scope.previewImage(filesId[0]);
                $scope.currentImage = filesId[0];
                // setTimeout(function() {$scope.$apply();}, 0);

                // 网络图片出错之后更新数据
                uploader.onImageError  = function(item) {
                    $('div.item[name='+item.localId+'] .img-preview').css({
                        'backgroundImage': 'url(' + item.data + ')'
                    });
                    $scope.data.imgs = uploader.getFilesUrl();
                    $scope.data.lastImgs = $scope.displayPreviewPanel ? uploader.getLastFiles() : [];
                };

                uploader.onComplete = function(localId,serverData) {
                    if(!$scope.thumb.url){
                        extractThumb();
                    }

                    updateImgInfo(localId,serverData.url);
                };

                uploader.upload();

                $timeout(function() {
                    $(window).trigger('resize');
                }, 0, false);
            });
            uploader.updateFileinfo($scope.data.lastImgs);
            uploader.setData(files, $scope.displayPreviewPanel);

            setTimeout(function() {
                $scope.$apply();
            }, 0);
        };
        $scope.trashImg = function(e, id) {
            e.preventDefault();
            e.stopPropagation();
            if ($scope.displayPreviewPanel) {
                renderImageList(); //正常化图片列表
            }
            uploader.trash(id);
            $(e.target).parents('.item').first().animate({
                opacity: 0,
                width: 0
            }, 300, function() {
                $(this).remove();


                $timeout(function() {
                    if ($scope.current && $scope.current.localId === id) {
                        if ($scope.data.imgs.length === 1) {
                            $scope.current = undefined;
                            $scope.currentImage = null;
                        } else {
                            $scope.data.imgs.forEach(function(item, index) {
                                var newIndex = index + 1;
                                if (item.localId === id) {
                                    if (newIndex === $scope.data.imgs.length) {
                                        newIndex = index - 1;
                                    }
                                    $scope.current = $scope.data.imgs[newIndex];
                                    $scope.currentImage = $scope.current.localId;
                                    return;
                                }
                            });
                        }

                        setTimeout(function() {
                            $scope.$apply();
                        }, 0);

                    }

                    $scope.data.imgs = uploader.getFilesUrl();
                    $scope.data.lastImgs = $scope.displayPreviewPanel ? uploader.getLastFiles() : [];

                }, 0);
            });
        };
        $scope.updateDescription = function() {
            uploader.updateFileinfo([$scope.current]);
            $('.gallery-panel').trigger('nlpService');
        };
        $scope.triggerNlp = function() {
            $('.gallery-panel').trigger('nlpService');
        };
        $scope.switch = function(page) {
            if(!$scope.current || $scope.data.imgs.constructor !== Array){
                return;
            }

            var index = $scope.data.imgs.indexOf($scope.current);
            index += page;

            if(index < 0 || index >= $scope.data.imgs.length){
                return;
            }

            $scope.current = $scope.data.imgs[index];
            $scope.currentImage = $scope.current.localId;
        };
        $scope.wordCount = function(data){
            if(!data || data.constructor !== String){
                return false;
            }
            return  Math.ceil(data.replace(/[\u1000-\uffff]{1}/g, 'aa').length / 2);
        };
        $scope.getThumb = function(str){
            str = str || '';
            if(str.indexOf('/assets/cloud/img/') > -1){return str;}
            return str.indexOf('.') < 0 ? str : str.replace(/^(\/[^\/]*\/)(.*)(\.[^\.]*)$/ig,function(a,b,c,d){return b+'thumb/'+c+d+'@w120_h90'+d});
        };

        // 图片预览，显示加载动画，但现在下面的缩略图其实是原图，预览时由于缓存加载几乎不费时
        $scope.$watch('current.dataUrl',function(n,o){
            if(n && o){
                loadimgTimer = $timeout(function(){
                    $scope.loadingImage = true;
                },20);
            }
        });
        $('img[bindLoad]').load(function(){
            $timeout(function(){
                loadimgTimer && $timeout.cancel(loadimgTimer);
                loadimgTimer = null;
                $scope.loadingImage=false;
            },0);
        });

        Sortable.create($('.imglist')[0], {
            animation: 150,
            filter: '.addimage',
            draggable: '.item',
            onEnd: function() {
                //更新排序结果
                var res = [];
                $('.imglist').find('.item').each(function(i, val) {
                    var id = $(val).attr('name') - 0;
                    !isNaN(id) && res.push(id);
                });
                uploader.updateSort(res);
                $scope.data.imgs = uploader.getFilesUrl();

                if($scope.currentImage){
                    $scope.current = $scope.data.imgs.filter(function(item){
                        return item.localId === $scope.currentImage;
                    })[0];
                }

                setTimeout(function(){
                    $scope.$apply();
                },0);
            }
        });
        $scope.initPreviewPanel = function() {
            //preview-childpage页面的摘要部分交互
            $('.gallery-preview-childpage').on('click', '.description', function(e) {
                $(e.currentTarget).find('textarea').focus();
                $(e.currentTarget).find('.textarea-outter').addClass('holdon');
            }).on('mouseleave', '.img-outter', function(e) {
                $(e.currentTarget).find('.textarea-outter').removeClass('holdon').scrollTop(0);
            }).on('keydown paste', '.description textarea', function(e) {
                if (e.type === 'keydown' && e.keyCode === 13) {
                    e.preventDefault();
                }
            }).on('keydown', function(e) {
                if (e.keyCode === 13 && $scope.thumburl.indexOf('.')) {
                    $scope.saveUrl($scope.thumburl);
                }
            });

            $scope.saveUrl = function(url) {
                $scope.pushFiles(url);
                $scope.openWholePanel = false;
                setTimeout(function() {
                    $scope.thumburl = '';
                }, 0);
            };

            //创建可拖拽排序效果
            var container = $('.gallery-preview-childpage .attachment-display-panel-inner');
            Sortable.create(container[0], {
                animation: 150,
                filter: '.filter',
                onStart: renderImageList,
                draggable: '.attachment-item',
                onEnd: function() {
                    //更新排序结果
                    var res = [];
                    container.find('.item').each(function(i, val) {
                        var id = $(val).attr('name') - 0;
                        !isNaN(id) && res.push(id);
                    });
                    uploader.updateSort(res);
                    uploader.updateFileinfo($scope.data.lastImgs);
                    $scope.data.lastImgs = $scope.displayPreviewPanel ? uploader.getLastFiles() : [];
                }
            });

            container.on('sizeChanged', function(e, w, h) {
                container.children('.upload-panel-outter').css({
                    width: w,
                    height: h
                });
            });
            $scope.stopPropagation = function(e){
                e.stopPropagation();
            };

            $scope.uploadImage = function() {
                $scope.uploading = true;

                //保存排序
                var items = container.find('.item');
                var res = [];
                items.each(function(i, val) {
                    var id = $(val).attr('name') - 0;
                    !isNaN(id) && res.push(id);
                });
                uploader.updateSort(res);

                //保存图说
                var commonDes = $scope.data.commonDescription;
                $scope.data.lastImgs.forEach(function(val) {
                    val.description = val.description || commonDes;
                });
                uploader.updateFileinfo($scope.data.lastImgs);

                // uploader.onComplete = function(){
                $scope.uploading = false;
                $scope.displayPreviewPanel = false;
                $scope.data.imgs = uploader.getFilesUrl();
                $scope.data.lastImgs = [];
                if ($scope.currentImage !== undefined) {
                    $scope.previewImage($scope.currentImage);
                };
                $('.gallery-panel').trigger('nlpService');

                setTimeout(function() {
                    $scope.$apply();
                }, 0);
                // };
            }
        };

        $http.get('/system/audit/getauditorstate').success(function(res) {
            $scope.auditinfoCache = res && res.data;

            if (!res.state || !res.data || !res.data.enableAudit) {
                switchViewsData(0); //默认状态
                return;
            }

            //获取审核配置信息
            if (res.data.mode == 1) {
                //全部送审
                $scope.data.enableAudit = true;
                $scope.view.enableAuditCheckbox = false;
            } else {
                //部分送审，显示选择复选框，复选框中的紧急应该是可以被禁用的
                $scope.view.enableAuditCheckbox = true;
            }

            //修改提示信息文案
            switchViewsData($scope.data.enableAudit ? 1 : 0);
        });

        //交互控制
        //添加面板滚动
        $('.gallery-panel .editor-container').on('scroll', function(e) {
            var titleElement = $('#gallery-add-title');
            var titleOffset,
                titleWidth;
            if (this.scrollTop > 45) {
                if (titleElement.css('position') === 'fixed') {
                    return;
                }
                titleOffset = titleElement.offset();
                titleWidth = titleElement.width();
                titleElement.css({
                    position: 'fixed',
                    left: '-500px',
                    top: '0',
                    marginLeft: '50%',
                    width: titleWidth + 'px',
                    zIndex: 15,
                    paddingTop: '3px',
                    paddingBottom: '3px',
                    backgroundColor: '#fff'
                });
                titleElement.children('textarea').css({
                    whiteSpace: 'nowrap',
                    fontSize: '20px',
                    lineHeight: '22px',
                    minHeight: '32px',
                    maxHeight: '32px'
                });
                titleElement.children('.editor-title-stat').addClass('hide');
                $(this).children().css('padding-top', '50px');
                //fixed之后滚动条消失
                //relative滚动条出现
            } else {
                // if(this.scrollTop === undefined || this.scrollTop === 0){return;};
                $(this).children().css('padding-top', '');
                if (titleElement.css('position') !== 'fixed') {
                    return;
                }
                titleElement.css({
                    position: 'relative',
                    left: '',
                    top: '',
                    width: '',
                    marginLeft: '',
                    zIndex: '',
                    paddingTop: '',
                    paddingBottom: ''
                });
                titleElement.children('textarea').css({
                    whiteSpace: 'inherit',
                    fontSize: '',
                    lineHeight: '',
                    minHeight: '',
                    maxHeight: ''
                });
                titleElement.children('.editor-title-stat').removeClass('hide')
                $('.article-panel').find('.top-panel').css({
                    borderBottomColor: '#fff'
                });
            }
        });

        //中间的描述样式控制
        $('textarea[ng-model="current.description"]').on('input', function() {
            if ($(this).height() > 35) {
                $(this).addClass('multi');
            } else {
                $(this).removeClass('multi');
            }
        });

        //自动提取摘要和关键词
        var manualChanged = {
            description: false,
            keyword: false
        };
        $('.keyword').on('keydown keyup click', 'input', function() {
            manualChanged.keyword = true;
            if (manualChanged.description === true) {
                $('.gallery-panel').off('nlpService');
            }
        });
        $('textarea.description').on('keydown keyup click', function() {
            manualChanged.description = true;
            if (manualChanged.keyword === true) {
                $('.gallery-panel').off('nlpService');
            }
        });
        $('.gallery-panel').on('nlpService', function() {
            var title = $scope.title;
            var content = '';
            var cache = [];
            if (!$scope.data.imgs || $scope.data.imgs.length === 0) {
                return;
            }
            $scope.data.imgs.forEach(function(item) {
                if (item.description && cache.indexOf(item.description) < 0) {
                    content += item.description;
                    cache.push(item.description);
                }
            });
            if(!content && !title){
                return;
            }

            $.post(api.autoabstractUrl, {
                title: title,
                content: content
            }).success(function(res) {
                if (!res.state) {
                    return;
                }
                res = res.data;
                /*if (!$scope.description || manualChanged.description === false) {
                 $scope.description = res.abstract && res.abstract.replace(/\n/gm, ' ');
                 }*/
                if (!$scope.keyword || manualChanged.keyword === false) {
                    $scope.keyword = res.keyword && res.keyword.toString();
                }
                $scope.$apply();
                $('textarea.description').trigger('update');
            });

            return;
        });


        $scope.title = $scope.content.title;
        $scope.thumb.id = $scope.content.thumbid;
        $scope.thumb.url = $scope.content.contentthumb;
        $scope.chooseThumbFromLibrary = true;


        /*加载图集*/
        function getContent(data){
            dataBak = data; //存储原始数据用于比较
            $scope.contentid = data.id;
            $scope.title = data.title;
            $scope.category = [];
            var forMatCategory = $scope.content.category.split(',');
            if (forMatCategory) {
                for (var i in forMatCategory) {
                    $scope.category.push(parseInt(forMatCategory[i]));
                }
            }
            $scope.thumb.id = data.thumbid;
            $scope.thumb.url = data.contentthumb || data.thumb;
            $scope.chooseThumbFromLibrary = true;
            $scope.source = data.source;
            $scope.sourceurl = data.sourceurl;
            $scope.channels = [
                {
                    name: "pc",
                    alias: "PC",
                    value: true,
                    tips: '发布到PC站'
                }, {
                    name: "phone",
                    alias: "手机",
                    value: true,
                    tips: '发布到客户端'
                },
                {
                    name: "wap",
                    alias: "WAP",
                    value: true,
                    tips: '发布到手机站'
                }
            ];

            /*$scope.data.imgs = [
             {
             dataUrl:"http://site.cmstop.cc/a/10001/201607/4bd8ad57edc08af6012b4bdf922831d8.png",
             description:"",
             localId:0,
             serverUrl:"http://site.cmstop.cc/a/10001/201607/4bd8ad57edc08af6012b4bdf922831d8.png"
             },
             {
             dataUrl:"http://site.cmstop.cc/a/10001/201607/4bd8ad57edc08af6012b4bdf922831d8.png",
             description:"",
             localId:1,
             serverUrl:"http://site.cmstop.cc/a/10001/201607/4bd8ad57edc08af6012b4bdf922831d8.png"
             }
             ]*/
            /*commonService.toJson(data.Content).forEach(function(item, g){
                var c = {};
                c['dataUrl'] = item.url;
                c['description'] = item.description;
                c['localId'] = item.id;
                c['serverUrl'] = item.url;

                $scope.data.imgs.push(c);
            });
            window.onload = function (){
                $('.img-outter').eq(0).trigger('click');
            }*/


                $scope.content = commonService.toJson(data.content);

                //处理网络数据
                uploader.ready(function(filesId) {
                    $scope.data.imgs = uploader.getFilesUrl();
                    $scope.data.lastImgs = $scope.displayPreviewPanel ? uploader.getLastFiles() : [];
                    $scope.previewImage(filesId[0]);
                    $scope.currentImage = filesId[0];

                    if (!$scope.thumb.url) {
                        uploader.onComplete = function() {
                            extractThumb();
                        };
                    } else {
                        uploader.onComplete = function() {};
                    }

                    $timeout(function() {
                        $(window).trigger('resize');
                    }, 0, false);
                });
                uploader.setServerData($scope.content);

                if($scope.auditinfoCache){
                    $scope.data.enableAudit = $scope.auditinfoCache.enableAudit ? ($scope.auditinfoCache.mode == 1 ? true : $scope.data.enableAudit) : false;
                }
                switchViewsData($scope.data.enableAudit ? 1 : 0);

               /* angular.forEach($scope.channels, function(val, i) {
                    if (data.channel && data.channel.indexOf(val.name) > -1) {
                        val.value = true;
                    } else {
                        val.value = false;
                    }
                });
                */
               // $scope.$apply();

                //调整样式
                $('textarea.description,textarea.editor-title').trigger('update');
                manualChanged.description
                if($scope.description){
                    manualChanged.description = true;
                }
                if($scope.keyword){
                    manualChanged.keyword = true;
                }
        }
        function compareData(data1,data2){
            var filterKeys = ['id','isdraft','draftid','enableAudit','isImportant'];
            var res = [];

            angular.forEach(data1,function(val,key){
                if(filterKeys.indexOf(key) > -1){
                    return;
                }

                if(!val || !data2[key]) {
                    if(val === '0'){val = 0;}
                    if(data2[key] === '0'){data2[key] = 0;}
                    if(data2[key] && data2[key].constructor===Array && data2[key].length === 0){
                        if(!!val){
                            res.push(key);
                        }
                        return;
                    }
                    if(!!val !== !!data2[key]){
                        res.push(key);
                    }
                    return;
                };

                if(key === 'thumb'){
                    if(val.id != data2[key].id){
                        res.push(key);
                    }
                    return;
                }

                if(val.constructor !== String && val.constructor !== Array){
                    if(val != data2[key]){
                        res.push(key);
                    }
                    return;
                }

                if(val.constructor === Array || data2[key].constructor === Array){
                    if(val.toString().split(',').sort().toString() !== data2[key].toString().split(',').sort().toString()){
                        res.push(key);
                    }
                    return;
                }

                if((val || '').toString() !== (data2[key] || '').toString()){
                    res.push(key);
                }
            });

            return res;
        }
        function collectData(){
            var data,channels = [],contentData;

            contentData = uploader.getAllFilesServerUrl();
            contentData.forEach(function(item){
                item.localId = undefined;
            });

            angular.forEach($scope.channels, function(item) {
                if (item.value) {
                    channels.push(item.name);
                }
            });

            //将页面中的数据格式化后保存到新的data中
            // 返回和提交数据都会用到
            data = {
                title: $scope.title || '',
                category: $scope.category,
                channel: channels,
                keyword: $scope.keyword,
                source: $scope.source || '',
                sourceurl: $scope.sourceurl || '',
                author: $scope.author,
                recommend: $scope.recommend,
                description: $scope.description,
                content: JSON.stringify(contentData),
                enableAudit: !!$scope.data.enableAudit,
                isImportant: !!$scope.data.isImportant,
                enablecopyright: $scope.enablecopyright ? 1 : 0
            };
            if ($scope.thumb && $scope.thumb.id) {
                data.thumb = {
                    id: $scope.thumb.id,
                    url: $scope.thumb.url.replace(/^http:\/\/[^\/]*/, '')
                };
            }
            if (typeof $scope.published !== 'undefined')
            {
                data.published = $scope.published
            }
            return data;
        }

        /*顶部标题*/
            getContent($scope.content);

        /*判断算是否是一篇新的内容*/
        if(query.id){
            $scope.isNewContent = false;
        }
        //  发布时间
        ;(function () {
            $scope.maxPublished = new Date();
            $scope.showDatepicker = function(){
                $('.published-panel .date-input').trigger("click");
            }
            function dateToStamp(dateStr)
            {
                if (typeof dateStr !== 'string') return dateStr;
                var S = dateStr.replace(/[-]/g,"/");
                var D = new Date(S).getTime();
                return D;
            }
            function foramt_date(pattern,stamp)
            {
                var D = new Date(stamp);
                var T = {};
                T['YYYY'] = D.getFullYear();
                T['MM'] = D.getMonth() + 1 > 9 ? D.getMonth() + 1: "0"+(D.getMonth() + 1);
                T['DD'] = D.getDate()  > 9 ? D.getDate(): "0"+(D.getDate());
                T['hh'] = D.getHours()  > 9 ? D.getHours(): "0"+(D.getHours());
                T['mm'] = D.getMinutes()  > 9 ? D.getMinutes(): "0"+(D.getMinutes());
                T['ss'] = D.getSeconds()  > 9 ? D.getSeconds(): "0"+(D.getSeconds());
                for (var i in T)
                {
                    pattern = pattern.replace(i,T[i])
                }
                return pattern;
            }
            $scope.$watch('_published',function (newVal,oldVal){
                var S = dateToStamp(newVal);
                if(S>0) $scope.published = foramt_date("YYYY-MM-DD hh:mm:ss",S);
            });
            $scope.$watch('published',function (newVal,oldVal){
                var S = dateToStamp(newVal);
                if(S>0) $scope._published = foramt_date("YYYY/MM/DD hh:mm",S)
            });
        })();

    }]);

});