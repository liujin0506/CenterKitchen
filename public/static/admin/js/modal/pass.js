(function() {
    'use strict';

    cloudModule.controllerProvider.register('politics-modal-pass-controller', [
        "$scope", "$http", "Modal", "commonService",
        function ($scope, $http, Modal, commonService) {

            $scope.deptlists = [];
            $scope.activeDept = {id:0,name:''};
            $scope.isOpen = $scope.$parent.content.anonymity;
            var hasCommit = false;
            //  获取部门列表
            $http.get($scope.appbaseurl+'setting/grouplist').success(function (res) {
                res = commonService.toJson(res);
                if (res.error) {
                    Modal.error(res.error);
                    return $scope.$close();
                }
                $scope.deptlists = res.data;
                $scope.getActiveDept($scope.$parent.content.groupid);
            });

            /**
             * 完成
             */
            $scope.complete = function () {
                if(hasCommit) return;
                hasCommit = true;
                $http.post($scope.appbaseurl+'Content/pass', {
                    id : $scope.$parent.content.id,
                    anonymity : $scope.isOpen,
                    groupid : $scope.activeDept.id,
                    checkData : {
                        audit: $scope.$parent.content.audit,
                        status: $scope.$parent.content.status,
                        anonymity: $scope.$parent.content.anonymity,
                        secede: $scope.$parent.content.secede,
                        groupid: $scope.$parent.content.groupid
                    }
                    
                }).success(function (res) {
                    res = commonService.toJson(res);
                    if (res.state) {
                        return $scope.$close({state:true, anonymity:$scope.isOpen, groupid:$scope.activeDept.id});
                    } else if(res.conflict) {
                        Modal.alert({
                            title:'操作冲突',
                            content:res.error,
                            buttons : [{
                                name: '确定',
                                cmd: 'ok',
                                event: function() {
                                    $scope.$close({
                                        conflict: true,
                                        state:false,
                                        data: res.data
                                    });
                                    
                                    return true;
                                }
                            }]
                        });
                    }
                });
            };
            //
            $scope.verify = function () {
                if ($scope.activeDept.id)
                    return 1;
                else
                {
                    Modal.error('请选择部门');
                    return 0;
                }
                    
            };
            //  获取当前部门
            $scope.getActiveDept = function(id)
            {
                $.each( $scope.deptlists,function (index,item) {
                    if (item.id == id)
                        $scope.activeDept = item;
                });

                return 1;
            };
            

        }
    ]);
}());