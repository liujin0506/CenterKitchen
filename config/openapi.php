<?php
/**
 * Description  CmsTop MediaCloud
 *
 * @Author      liujing <liujing@cmstop.com>
 * @DateTime    2018/4/28 15:52
 * @CopyRight   Beijing CmsTop Technology Co.,Ltd.
 */

return [
    'openurl' => env('OPEN_API_URL', ''),
    'appid' => env('OPEN_API_APPID', ''),
    'secret' => env('OPEN_API_SECRET', '')
];