(function(){
    'use strict';

    /**
     * 输入框输入格式化
      */
    cloudModule.filterProvider.register('valueDef', function () {
        
        return function (val, defaultVal) {
            var excluVal = ['',' '],ret = val;
            excluVal.push(void(0));
            if (excluVal.indexOf(val)>-1 || /^\s*$/.test(val))
                ret = defaultVal;
            return ret;
        }
    });
    /**
     * 字符串截取
     */
    cloudModule.filterProvider.register('cutstr', function () {
        
        return function (val,args) {
            
            var ret, _args, len, etc;
            _args = typeof args === 'Array' ? args : [args];

            len = _args[0];
            etc = isset(_args[1])? _args[1] : '...';
            ret = textLen(val)<=len ? val : subText(val,len)+etc;
            return ret;
        }
    });

    /**
     * 截取字符串长度，单字节字符算半个长度
     * 
     * @param text 
     * @param len 截取长度
     * @param offset  开始位置
     * @returns {*}
     */
    function subText(text,len,offset)
    {
        var _len,_offset,_text,realLen;
        
        _len = textLen(text);
        
        if(_len<= len)
        {
            return text;
        }
        
        _offset     = isset(arguments[2]) ? arguments[2] : 0;
        _text       = text.substr(_offset,len);
        realLen     = textLen(_text);
        
        if(realLen==0)
        {
            _text = text.substr(_offset,len+1);
            realLen = textLen(_text);
        }
        if(realLen<len)
        {
            _text += subText(text,len-realLen,_offset+len);
        }
        
        return _text;
    }

    /**
     * 计算字符串长度，单字节字符算半个长度，一个单字节返回值为0
     * 
     * @param text
     * @returns {*}
     */
    function textLen(text)
    {
        var len,singleLen, multiLen, realLen, regSingleChar;

        regSingleChar = /[\w\d\,\\\/\.\'\"\:\;\+\=\_\-\[\]\}\{\s]+?/img ;
        len = text.length;
        multiLen = text.replace(regSingleChar,'').length;
        singleLen = len - multiLen;
        realLen = multiLen + Math.floor(singleLen/2);

        return realLen;
    }

    function isset(obj)
    {
        return typeof obj !== 'undefined' ? true : false;
    }
    
})();