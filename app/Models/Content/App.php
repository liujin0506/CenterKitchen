<?php
/**
 * Description  CmsTop MediaCloud
 *
 * @Author      liujing <liujing@cmstop.com>
 * @DateTime    2018/4/28 14:43
 * @CopyRight   Beijing CmsTop Technology Co.,Ltd.
 */

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $table = 'app';

    public function lists()
    {
        $query = static::query()->select(['id']);
        $limit = 10;
        $pageData = $query->toSql();

        return $pageData;
    }
}