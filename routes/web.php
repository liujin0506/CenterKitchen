<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->namespace('Admin')->group(function (){
    Route::get('content/livelist', [
        'uses' => 'Content\UserController@showProfile',
        'route_name' => '获取个人信息',
        'route_log' => false,
        'route_validate_class' => 'role',
        'route_validate_scene' => 'create'
    ]);
});