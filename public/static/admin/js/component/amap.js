(function(){
	"use strict";
	define([
    // 高德API(Web端) key：e2effa2db3d0f6cc6c8952833e44a19b
    "http://webapi.amap.com/maps?v=1.3&key=e2effa2db3d0f6cc6c8952833e44a19b",

    // cloud modal
    "/assets/cloud/ui/modal/modal.js",
	],function(){
    cloudModule.compileProvider.directive('amap',
      ['$http','$timeout','Modal','commonService',
      function($http,$timeout,Modal,commonService){
        return {
          restrict:'AE',
          scope:{
            amap:'=',
            isactive:'='
          },
          link:function($scope,$element,$attr){
            var url = {
              list_task:"/" + remoteAppId_NTGJ + '/dispatch/center?action=task',
            };

            // 初始化城市名
            var cityNameInit = function(name){
              if(name.charAt(name.length - 1) === '市'){
                return name.substr(0,name.length - 1);
              }
              return name;
            };

            // amap init
            var amap = new AMap.Map('amap_wrapper',{
              zoom:11,
              // 标准版：amap://styles/4a1961814f9888a2d2a9db4f384b0bcb
              // 银月黑: amap://styles/light
              mapStyle: 'amap://styles/4a1961814f9888a2d2a9db4f384b0bcb'
            });
            amap.on('moveend',function(){
                amap.getCity(function(result){
                    $timeout(function(){
                        var _zoom = amap.getZoom();
                        if(_zoom >= 9){
                            $scope.amap.currentPosition = cityNameInit(result.city||result.province);
                        }else if(_zoom >= 6){
                            $scope.amap.currentPosition = cityNameInit(result.province);
                        }else{
                            $scope.amap.currentPosition = '全国';
                        }
                    });
                });
            });

            // 自动获取用户ip地址
            var userPosition = {
              status:'user',
              lon:'',
              lat:''
            };
            amap.plugin('AMap.Geolocation',function(){
              var geolocation = new AMap.Geolocation({
                enableHighAccuracy: true,//是否使用高精度定位，默认:true
                timeout: 10000,          //超过10秒后停止定位，默认：无穷大
                maximumAge: 0,           //定位结果缓存0毫秒，默认：0
                convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
                showButton: true,        //显示定位按钮，默认：true
                buttonPosition: 'LB',    //定位按钮停靠位置，默认：'LB'，左下角
                buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
                showMarker: true,        //定位成功后在定位到的位置显示点标记，默认：true
                showCircle: true,        //定位成功后用圆圈表示定位精度范围，默认：true
                panToLocation: true,     //定位成功后将定位到的位置作为地图中心点，默认：true
                zoomToAccuracy:true      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
              });
              // amap.addControl(geolocation);
              geolocation.getCurrentPosition();
              AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
              AMap.event.addListener(geolocation, 'error', onError);      //返回定位出错信息
              function onComplete(data){
                // 经度：data.position.getLng()
                // 纬度：data.position.getLat()
                var _params = {
                  location:
                    (data.addressComponent.street ? 
                    data.addressComponent.street + 
                    data.addressComponent.streetNumber : 
                    data.addressComponent.city + 
                    data.addressComponent.district),
                  lon:data.position.getLng(),
                  lat:data.position.getLat()
                };
                angular.extend(userPosition,_params);
                $scope.$apply(function(){
                  $scope.amap.currentPosition = cityNameInit(data.addressComponent.city);
                  $scope.amapList.viewHandle();
                });
              }
              function onError(data){
                userPosition = {};
                new Modal({
                  title:'获取您的位置失败',
                  content:data
                }).open();
                $scope.$apply(function(){
                  $scope.amap.currentPosition = '(定位失败)';
                });
              }
            });

            // 搜索city
            amap.plugin('AMap.Autocomplete',function(){
              $scope.amap.searchInit = function(){
                var _ele = $("#search_city_input");
                var _timeid;
                var autoOptions = {
                  // type：高德Poi类型编码 直辖市和地级市190104
                  type:'190103|190104',
                  city:'',
                  input:'search_city_input',
                  datatype:'poi'
                };
                var autocomplete = new AMap.Autocomplete(autoOptions);
                var _event = function(res){
                  if(res.poi && res.poi.name){
                    setCity(res.poi.name);
                    setTimeout(function(){
                      _ele.val('');
                      $(".amap-sug-result").hide();
                    });
                  }
                };
                // 下拉选取事件
                AMap.event.addListener(autocomplete,'select',_event);
                // enter事件
                _ele[0].addEventListener('keydown',function(e){
                  if(_timeid){
                    clearTimeout(_timeid);
                    _timeid = null;
                  }
                  _timeid = setTimeout(function(){
                    var _val = _ele.val();
                    if(e.keyCode === 13 && _val !== ''){
                      autocomplete.search(_val,function(status,result){
                        if(status === 'complete'){
                          _event({
                            poi:{name:result.tips[0].name}
                          });
                        }
                      });
                      return false;
                    }
                  },200);
                },true);
              };
            });

            // turn list
            $scope.amapList = {
              data:[],
              timeSpace:45000,
              timeId:null,
              animateTimeId:null,
              viewHandle:function(){
                var _this = $scope.amapList;
                var _html = '';
                var _marker;
                var _realData = [];
                var _data = angular.copy(_this.data);
                // 清除地图覆盖物
                amap.clearMap();
                // 新增点标记
                _data.unshift(userPosition);
                angular.forEach(_data,function(item){
                  if(item.location === -1 ||
                  !item.lon || !item.lat){
                    return;
                  }
                  // task
                  if(typeof item.status === 'number'){
                    item.data = {
                      id:item.id,
                      type:'task'
                    };
                    _html = 
                      "<div class='amap-person-tab-title'>" + 
                      item.title + "</div>" +
                      "<div class='amap-person-tab-number p-status p-status-" + item.status + "'>" + 
                      (item.status === 3?'待处理':item.status === 4?'进行中':'已完成') + "</div>";
                  }
                  // person
                  if(typeof item.memberid !== 'undefined'){
                    item.status = item.tasknum?1:0;
                    item.data = {
                      location:item.location,
                      author:item.author,
                      tasknum:item.tasknum,
                      memberid:item.memberid,
                      type:'person'
                    };
                    _html = 
                      "<div class='amap-person-tab-user'></div>" + 
                      "<div class='amap-person-tab-name'>" + 
                      item.author + "</div>" +
                      "<div class='amap-person-tab-position'></div>" +
                      "<div class='amap-person-tab-location'>" + 
                      item.location + "</div>" +
                      "<div class='amap-person-tab-number'>" + 
                      item.tasknum + "</div>";
                  }
                  // user own
                  if(item.status === 'user'){
                    item.data = {
                      id:item.id,
                      type:'user'
                    };
                    _html = 
                      "<div class='amap-person-tab-name'>我在</div>" +
                      "<div class='amap-person-tab-location'>" + 
                      item.location + "</div>" +
                      "<div class='amap-person-tab-number'>附近</div>";
                  }
                  item.marker = {
                    // icon:new AMap.Icon({
                    //   size:new AMap.Size(30,30),
                    //   image:"/" + remoteAppId_NTGJ + "/res/images/task_pending.png"
                    // }),
                    content:
                      "<div class='amap-person-tab-marker p-icon-" + 
                        item.status + "'><div class='animate-pos'></div>" + 
                        "<div class='amap-person-tab'><div class='p-triangle'></div>" + 
                        _html +
                        "</div>" +
                      "</div>",
                    position:[item.lon,item.lat],
                    // offset:new AMap.Pixel(-0, -42),
                    map:amap,
                    extData:item.data,
                  };
                  _marker = new AMap.Marker(item.marker);
                  item.marker = _marker;
                  _realData.push(item);
                  AMap.event.addListener(_marker,'click',_this.markerClick);
                });
                $scope.amap.search.init(_realData);
                _this.timeTurn();
                // amap.setFitView();
              },
              change:function(){
                var _this = $scope.amapList;
                if(_this.timeId){
                  $timeout.cancel(_this.timeId);
                  _this.timeId = null;
                }
                _this.dataHandle();
              },
              timeTurn:function(){
                var _this = $scope.amapList;
                if(_this.timeId){
                  $timeout.cancel(_this.timeId);
                  _this.timeId = null;
                }
                _this.timeId = $timeout(function(){
                  _this.dataHandle();
                  _this.timeId = null;
                },_this.timeSpace);
              },
              dataHandle:function(){
                if($scope.isactive !== 11) return;
                var _this = $scope.amapList;
                if(_this.animateTimeId){
                  $timeout.cancel(_this.animateTimeId);
                  _this.animateTimeId = null;
                }
                $scope.amap.loading = true;
                var _params = $scope.amap.params;
                $http.get(url.list_task,{params:_params}).then(function(res){
                  _this.animateTimeId = $timeout(function(){
                    $scope.amap.loading = false;
                  },450);
                  try{
                    var _result = commonService.toJson(res.data);
                    var _data;
                    if(_result && _result.state){
                      _data = [].concat(_result.data || [],_result.rlist);
                      if(angular.equals(_data,_this.data)){
                        _this.timeTurn();
                        return;
                      }
                      _this.data = _data;
                      _this.viewHandle();
                    }else{
                      _this.timeTurn();
                    }
                  }catch(e){
                    console.log(e);
                    _this.timeTurn();
                  }
                });
              },
              markerClick:function(e){
                var _event,_params;
                var item = e.target.getExtData();
                switch(item.type){
                  case 'user':
                    _params = 1;
                    _event = $scope.amap.taskListChange;
                    break;
                  case 'task':
                    _params = item;
                    _event = $scope.amap.taskContent;
                    break;
                  case 'person':
                    _params = item;
                    _event = $scope.amap.personInfo;
                    break;
                  default:
                }
                $scope.$apply(function(){
                  _event(_params);
                });
              }
            };

            // 外部定位服务
            function setCity(cityname,xy,adcode){
              var _city = cityNameInit(cityname);
              if(xy){
                var _zoom = 10;
                var _pos = xy;
                if(_city === '全国'){
                    _zoom = 4;
                }
                amap.setZoomAndCenter(_zoom,_pos);
              }else{
                $scope.amap.loading = true;
                amap.setCity(adcode||_city,function(){
                    $timeout(function(){
                        $scope.amap.loading = false;
                    },300);
                });
              }
              $timeout(function(){
                $scope.amap.currentPosition = _city;
                $scope.amap.setCityCallback();
              });
            }
            $scope.amap.setCity = setCity;

            // 监听用完就销毁
            var _watch_amap_params;
            $scope.amap.clear = function(){
              $scope.amap.searchClear();
              $scope.amap.search.clear();
              _watch_amap_params && _watch_amap_params();
            };
            $scope.amap.init = function(){
              // $scope.amapList.change();
              $scope.amap.change();
              $timeout(function(){
                _watch_amap_params = $scope.$watch('amap.params',function(){
                  $scope.amapList.change();
                });
              });
            };

            // 搜索
            $scope.amap.setCenter = function(arr){
              amap.setZoomAndCenter(14,[arr[0].lon,arr[0].lat]);
              angular.forEach(arr,function(item){
                item.marker.setAnimation('AMAP_ANIMATION_BOUNCE');
              });
              $timeout(function(){
                angular.forEach(arr,function(item){
                  item.marker.setAnimation('AMAP_ANIMATION_NONE');
                });
              },2400);
            };
            
          }
        };
      }]
    );
  });
}());