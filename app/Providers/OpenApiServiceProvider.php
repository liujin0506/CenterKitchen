<?php
/**
 * Description  CmsTop MediaCloud
 *
 * @Author      liujing <liujing@cmstop.com>
 * @DateTime    2018/4/28 15:45
 * @CopyRight   Beijing CmsTop Technology Co.,Ltd.
 */

namespace App\Providers;

use GuzzleHttp\Client;

class OpenApiServiceProvider
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $api;

    /**
     * @var string
     */
    protected $appid;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var string
     */
    protected $error;

    /**
     * 初始化
     *
     * @param $api
     * @param $appid
     * @param $secret
     */
    public function __construct($api = '', $appid = '', $secret = '')
    {
        $this->api = $api ?: config('openapi.openurl');
        $this->appid = $appid ?: config('openapi.appid');
        $this->secret = $secret ?: config('openapi.secret');
        $this->client = new Client();
    }

    /**
     * 发起请求
     * @param $method
     * @param $uri
     * @param array $params
     * @return bool|mixed
     */
    public function request($method, $uri, $params = [])
    {
        $config = ['http_errors' => false];
        $options = [];
        $url = $this->url($uri, $params);

        if ((strtoupper($method) == "POST" || strtoupper($method) == "PUT") && !empty($params)) {
            $options = ['form_params' => $params];
        }
        try {
            $result = $this->getHttpClient($config)->request($method, $url, $options);
            $ret = $result->getBody()->getContents();
            $this->parseResponse($ret);
            return $this->data;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $this->error = $e->getMessage();
            $this->data = false;
            return false;
        }
    }

    /**
     * Get a fresh instance of the Guzzle HTTP client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function getHttpClient($config)
    {
        return new Client($config);
    }

    /**
     * 错误提示
     * @access public
     *
     * @return string
     */
    public function error()
    {
        return $this->error;
    }

    /**
     * 处理返回结果
     * @access protected
     *
     * @param $response
     */
    protected function parseResponse($response)
    {
        try {
            $result = json_decode($response, true);
        } catch (\Exception $e) {}

        if (json_last_error() != JSON_ERROR_NONE) {
            $this->error = $response;
            $this->data = false;
            return;
        }

        if (empty($result) || !is_array($result)) {
            $this->error = $response;
            $this->data = false;
            return;
        }

        if (!$result['state']) {
            $this->error = !empty($result['error']) ? $result['error'] : 'Unknown error';
            $this->data = false;
            return;
        }

        $this->data = $result['data'];
    }

    /**
     * 生成签名URL
     * @access public
     *
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function sign($url = '', array $params)
    {
        $time = time();
        $params['time'] = $time;
        $params['appid'] = $this->appid;
        ksort($params);
        $sign = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        $params['sign'] = md5(md5($sign) . $this->appid . $this->secret . $time);

        return $url . '?' . http_build_query($params);
    }

    /**
     * 外部调用生成签名后的URL
     * @access public
     *
     * @param string $api
     * @param array $params
     * @return string
     */
    public function url($api = '', Array $params = array())
    {
        $url = rtrim($this->api, '/') . '/' . ltrim($api, '/');
        return $this->sign($url, $params);
    }
}