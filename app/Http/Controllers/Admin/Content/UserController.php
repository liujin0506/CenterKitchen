<?php
/**
 * Description  CmsTop MediaCloud
 *
 * @Author      liujing <liujing@cmstop.com>
 * @DateTime    2018/4/27 19:20
 * @CopyRight   Beijing CmsTop Technology Co.,Ltd.
 */

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use App\Models\Content\App;
use App\Providers\OpenApiServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    public function showProfile(App $content)
    {
        $Client = new OpenApiServiceProvider();
        $ret = $Client->request("POST", "FetchData/site", [
            "type" => 'cloud_data',
            "sql_data" => base64_encode("SELECT * FROM aca limit 1"),
            "siteid" => 10001
        ]);
        if ($ret) {
            return $ret;
        } else {
            return $Client->error();
        }
    }
}