(function(){
	"use strict";
	define([
    "/assets/cloud/ui/scrollbar/scrollbar.js",
	],function(){
		cloudModule.compileProvider.directive('citySearch',function($http,$timeout,commonService){
			return {
        restrict:'AE',
        replace:true,
				scope:{
          amaplist:'='
        },
        templateUrl:"/" + remoteAppId_NTGJ + "/res/js/component/city_search/city_search.html",
        link:function($scope,$element,$attr){
          var url = {
            city_list:"/" + remoteAppId_NTGJ + "/dispatch/citylist"
          };
          // options
          $scope.options = {
            scroll:JSON.stringify({
              scrollSpeed:0.2
            }),
          };
          // 城市列表
          $scope.cityList = {
            type:'province',
            data:[],
            active:false,
            state:false,
            localStrName:'amap_city_list',
            localStrGet:function(){
              var _this = $scope.cityList;
              var _res = window.localStorage.getItem(_this.localStrName);
              if(_res){
                return _res;
              }
              return false;
            },
            localStrSet:function(){
              var _this = $scope.cityList;
              window.localStorage.setItem(_this.localStrName,JSON.stringify(_this.data));
            },
            handle:function(){
              var _this = $scope.cityList;
              var _res = _this.localStrGet();
              if(_res){
                _this.data = commonService.toJson(_res);
                _this.state = true;
                return;
              }else{
                $http.get(url.city_list).success(function(res){
                  var _result = commonService.toJson(res);
                  if(_result && _result.status && _result.data){
                    _this.data = _result.data;
                    _this.localStrSet();
                    _this.state = true;
                  }
                });
              }
            },
            slide:function(index){
              var _this = $scope.cityList;
              var _offsetTop = 0;
              if(!_this.slideBtn){
                _this.slideBtn = {
                  province:angular.element("#cityProvince"),
                  city:angular.element("#cityCity")
                };
              }
              _offsetTop = _this.slideBtn[_this.type].find("li[data-index='index_" + index + "']")[0].offsetTop;
              _this.slideBtn[_this.type].animate({
                scrollTop:_offsetTop
              },400);
            },
            setCityCallback:function(){
              var _this = $scope.cityList;
              _this.active = false;
            },
            setCity:function(cityname,xy,adcode){
              var _this = $scope.cityList;
              $scope.amaplist.setCity(cityname,xy,adcode);
            }
          };
          $scope.cityList.handle();
          // init search
          $scope.amaplist.searchInit();
          $scope.amaplist.setCityCallback = $scope.cityList.setCityCallback;
          $scope.amaplist.searchClear = function(){
            $scope.cityList.active = false;
          };

        }
      };
    });
  });
}());