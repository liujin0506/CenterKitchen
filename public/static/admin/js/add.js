var remoteAppId_NTGJ = window.location.hash.replace(/^#\/|\/$/g, '');
/*global $:true*/
/*global cloudModule:true*/
(function() {
    'use strict';

    cloudModule.controllerProvider.register('collect-admin-add-controller', 
    ['$scope', 'Modal', '$http', '$location', '$timeout', 'commonService',
      function($scope, Modal, $http,$location, $timeout, commonService) {
        var editor, backModal, submitSuccessModal, submitFailedModal, draftFailedModal, autoabstractUrl, nlpHanddler;
        //记录摘要和关键词是否被手动输入，bool值
        var descripttionState = true,
            keywordsState = true,
            titleChanged, contentChanged, contentBak = '',
            titleBak = '',
            deltaWord = 5;

        var descriptionDom = $('textarea.description');
        var submitBtn = $('.submit');
        var submiting = false;
        var scroll1, scroll2;
        var initEditor;
        var query = $location.search();
        var dataBak;
        $scope.content = $scope.editContent.content;
		$scope.template =  {
                welcome : "/" + remoteAppId_NTGJ + "/res/template/welcome.html",
                normal : "/" + remoteAppId_NTGJ + "/res/template/normal.html",
                add : "/" + remoteAppId_NTGJ + "/res/template/add.html"
            };


        function switchViewsData(mode) {
            if (mode === 1) {
                //审核
                $scope.view.publishBtn = '发稿';
                $scope.view.successModalTitle = '已发稿';
                $scope.view.successModalContent = '已进入待审，需要审核通过后才会发布';
                $scope.view.failModalTitle = '发稿失败';
                $scope.view.disablePreview = true;
                $scope.view.enablePriority = 1;
            } else {
                //不审核
                $scope.view.publishBtn = '发布';
                $scope.view.successModalTitle = '发布成功';
                $scope.view.failModalTitle = '发布失败';
                $scope.view.successModalContent = ' ';
                $scope.view.disablePreview = false;
                $scope.view.enablePriority = $scope.view.enableAuditCheckbox ? 2 : 0;//0表示隐藏，2表示禁用，1表示开启
            }
        }

        cloud.selectedMenu = [];
        $scope.existsRepeatContent = false; // 新增重复检测status
        $scope.titleNum = 36;
        $scope.isNewContent = true;
        $scope.description = '';
        $scope.submitValid = false;
        $scope.thumb = {};
        $scope.publishState = 'unpublish';
        $scope.chooseThumbFromLibrary = false;
        $scope.view = {
            enablePriority: 0
        }; //界面显示文字
        $scope.data = {};
        $scope.data.attachments = [];
        $scope.appid = 1;
		autoabstractUrl = '/article/index/prenlp';

        descriptionDom.add('textarea.editor-title').on('keydown click paste keyup update', function(e) {
            $(this).next().html($(this).html());
            if (e.type === 'paste') {
                $(this).next().html($(this).html() + e.originalEvent.clipboardData.getData("text"));
            }
            if (e.type !== 'update') {
                descripttionState = false;
            }
            if (e.type === 'keydown' && (e.keyCode > 65 && e.keyCode < 90) || (e.keyCode > 48 && e.keyCode < 57) || e.keyCode === 32 || (e.keyCode > 186 && e.keyCode < 191) || e.keyCode === 222) {
                $(this).next().html($(this).html() + 's');
            }
            if ($(this).hasClass('description')) {
                $(this).height($(this).next().height() + 29);
            } else {
                $(this).height($(this).next().height());
            }
            if (e.keyCode === 13) {
                //将enter键替换为空格
                if ($(this).hasClass('description')) {
                    $scope.description += ' ';
                } else {
                    if ($scope.title !== '') {
                        tinymce.execCommand('mceFocus', false, 'editor');
                    }
                }
                $scope.$apply();
                e.preventDefault();
                return false;
            }
        });

        //自动计算摘要
        $('textarea.editor-title').on('blur', function() {
            if (titleBak !== $scope.title) {
                if (!editor || editor && wordNum(editor.getContent({
                        format: 'text'
                    })) < 100) {
                    return;
                }
                $(document).trigger('nlpService');
            }
            titleBak = $scope.title;
        });
        /* 滚动条 */
        $('.sider-container,.editor-container').niceScroll({
            boxzoom: false,
            cursorwidth: 8,
            cursoropacitymax: 0.6,
            background: 'none',
            scrollspeed: 100,
            horizrailenabled: false,
            zindex: 10
        });
        $('.editor-container').on('scroll', function(e) {
            var titleElement, toolbarElement,
                titleOffset, toolbarOffset,
                titleWidth, toolbarWidth;
            titleElement = $('#article-add-title');
            toolbarElement = $('.mce-toolbar-grp');
            if (this.scrollTop > 32) {
                if (titleElement.css('position') === 'fixed') {
                    return;
                }
                titleOffset = titleElement.offset();
                toolbarOffset = toolbarElement.offset();
                titleWidth = titleElement.width();
                toolbarWidth = $('.editor-container-inner').width();
                titleElement.css({
                    position: 'fixed',
                    left: '-510px',
                    top: '0',
                    marginLeft: '50%',
                    width: titleWidth + 'px',
                    zIndex: 15,
                    paddingTop: '9px',
                    paddingBottom: '3px',
                    backgroundColor: '#fff'
                });
                $('.article-panel').find('.top-panel').css({
                    borderBottomColor: '#eee'
                });
                titleElement.children('textarea').css({
                    whiteSpace: 'nowrap',
                    fontSize: '20px',
                    lineHeight: '22px',
                    minHeight: '0',
                    maxHeight: '32px'
                });
                titleElement.children('.editor-title-stat').addClass('hide');
                toolbarElement.css({
                    position: 'fixed',
                    left: '-510px',
                    marginLeft: '50%',
                    top: '45px',
                    width: toolbarWidth + 'px',
                    height: '33px'
                });
                $(this).css('padding-top', '80px');
            } else {
                $(this).css('padding-top', '');
                if (titleElement.css('position') !== 'fixed') {
                    return;
                }
                titleElement.css({
                    position: 'relative',
                    left: '',
                    top: '',
                    width: '',
                    marginLeft: '',
                    zIndex: '',
                    paddingTop: '',
                    paddingBottom: ''
                });
                titleElement.children('textarea').css({
                    whiteSpace: 'inherit',
                    fontSize: '',
                    lineHeight: '',
                    minHeight: '',
                    maxHeight: ''
                });
                titleElement.children('.editor-title-stat').removeClass('hide')
                toolbarElement.css({
                    position: 'absolute',
                    left: '',
                    top: '',
                    width: '',
                    marginLeft: '',
                    height: ''
                });
                $('.article-panel').find('.top-panel').css({
                    borderBottomColor: '#fff'
                });
            }
        });
        /* 编辑器 */
        $scope.titleDeplicate = [];
        $scope.autofill = false;
        $scope.$watch('title', function(title) {
            $scope.title = $scope.title && $scope.title.replace(/\n/gm, ' ');

            function temp() {
                $("textarea.editor-title").trigger("update");
            }
            setTimeout(temp, 0);
        });
        $scope.$watch('description', function(description) {
            $scope.description = $scope.description && $scope.description.replace(/\n/gm, ' ');

            function temp() {
                descriptionDom.trigger('update');
            }
            setTimeout(temp, 0);
        });

        initEditor = function() {
			tinyMCE.editors = [];
			tinyMCE.activeEditor=null;
            $('#editor').editor('article', {
                width: '100%',
                autoresize_min_height: 276,
                watermark: true,
                init_instance_callback: function(ed) {
                    var bgPlaceholder = '/assets/module/article/index/css/image/placeholder-Content.png',
                        bgPlaceholder2x = '/assets/module/article/index/css/image/placeholder-Content@2x.png';

                    editor = ed;
                    $('#editor').removeClass('hide');

                    editor.setContent($scope.content.content);
                    if (!editor.getContent({
                            format: 'text'
                        }).replace('{{Content}}', '')) {
                        editor.setContent('');
                        editor.dom.doc.body.style.backgroundImage = 'url(' + bgPlaceholder + ')';
                    }
                    editor.dom.doc.body.style.backgroundRepeat = 'no-repeat';
                    editor.dom.doc.body.style.backgroundPosition = '0 8px';

                    editor.on('focus', function() {
                        $(editor.editorContainer).addClass('tinymce-focus');
                        editor.dom.doc.body.style.backgroundImage = '';
                        $(document).click();
                    }).on('blur', function() {
                        var contentText, imgs;
                        //计算字数
                        //判断自动计算的条件是否满足，并触发自动计算功能
                        contentText = editor.getContent({
                            format: 'text'
                        });
                        if (wordNum(contentText) > 100 && Math.abs((wordNum(contentBak) - wordNum(contentText))) > deltaWord) {
                            $(document).trigger('nlpService');
                        };
                        contentBak = contentText;
                        //自动提取缩略图
                        getthumb();
                        $(editor.editorContainer).removeClass('tinymce-focus');
                        if (!editor.getContent()) {
                            editor.dom.doc.body.style.backgroundImage = 'url(' + bgPlaceholder + ')';
                            editor.dom.doc.body.style.backgroundRepeat = 'no-repeat';
                            editor.dom.doc.body.style.backgroundPosition = '0 8px';
                        }
                    }).on('change', function() {
                        if (editor.getContent()) {
                            editor.dom.doc.body.style.backgroundImage = '';
                        }
                    }).on('remote2localSuccess', function(event) {
                        var imgdata = event[0];
                        var contentText = editor.getContent({
                            format: 'text'
                        });
                        getthumb();
                        if ($scope.keyword) {
                            setImgTags({
                                id: imgdata.attachid,
                                alias: imgdata.alias,
                                tags: $scope.keyword
                            });
                        } else {
                            if (wordNum(contentText) <= 100) {
                                return; //不满足自动计算的条件
                            };
                            getdesciption($scope.title, contentText).then(function() {
                                setImgTags({
                                    id: imgdata.attachid,
                                    alias: imgdata.alias,
                                    tags: $scope.keyword
                                });
                            });
                        }
                    }).on('paste', function() {
                        var currentDom = $('i.mce-i-formatclear').parents('div.mce-btn');
                        createTooltip(currentDom);
                        currentDom.on('mouseenter', function() {
                            $('#mce-temp-tooltip').length && $('#mce-temp-tooltip').remove();
                        });
                        setTimeout(function() {
                            $('#mce-temp-tooltip').length && $('#mce-temp-tooltip').remove();
                        }, 3000);
                    });
                    $(editor.dom.doc).find('html').bind('DOMMouseScroll', function(event) {
                        $('.editor-container').scrollTop($('.editor-container').scrollTop() + event.originalEvent.detail * 14);
                    });
                }
            });
        };

        /**
         * 初始化编辑器
         */
        $scope.initEditor = function () {
            tinyMCE.remove('#editor');
            tinyMCE.activeEditor = null;
            $timeout(function () {
                $("#editor").editor('article', { // cteditor
					width: '100%',
                    autoresize_min_height: 270,
                    //autoresize_max_height: 800,
                    init_instance_callback: function (edt) {
						editor = edt;
                        tinyMCE.activeEditor.setContent($scope.content.content);
                        var tinymceElement = $(edt.contentAreaContainer).parents('.mce-tinymce');
                        var tinymceContainer = tinymceElement.parents('.editor-container');
                        $(edt.contentAreaContainer).find('iframe')[0].contentDocument.body.style.paddingBottom = 0;
                        $(edt.contentAreaContainer).find('iframe')[0].contentDocument.body.style.minHeight = '100%';
						edt.on('focus', function() {
                            tinymceElement.addClass('tinymce-focus');
                        }).on('blur', function() {
                            tinymceElement.removeClass('tinymce-focus');
                            getthumb();
                        }).on('change',function(){
                            //editHasModify = true;
                        }).on('remote2localSuccess', function(event) {
                            getthumb();
                        });
                        tinymceContainer.scroll(function (e) {
                            tinymceElement.find('.mce-toolbar-grp').css('top', tinymceContainer.scrollTop() - 5);
                        });

                    }
                });
            }, 500);
        };



        if ($('#editor').editor) {
            initEditor();
        } else {
            $(document).on('tinyMCE_loaded', initEditor);
        }
        $scope.wordCount = function(str) {
            return str ? Math.ceil(str.replace(/[\u1000-\uffff]{1}/g, 'aa').length / 2) : 0;
        };

        /* 发布 */
        submitSuccessModal = function() {
            var buttons = [{
                    name: '返回',
                    event: function() {
                        // ------------------------- /
                        // 云稿库iframe调用检测
                        if (parent && parent.cloud.superContent) {
                            parent.location.reload(); return;
                        }
                        // ------------------------- /
                        return true;
                    }
                }];
            if(!$scope.view.disablePreview){
                buttons.push({
                    name: $scope.view.disablePreview ? '' : '查看',
                    cmd: 'view',
                    event: function() {
                        return true;
                    }
                });
            }
            return new Modal({
                type: 'success',
                title: $scope.view.successModalTitle,
                content: $scope.view.successModalContent,
                buttons: buttons
            });
        };
        submitFailedModal = new Modal({
            type: 'error',
            title: $scope.view.failModalTitle, content: ''
        });
        draftFailedModal = new Modal({
            type: 'error',
            title: '存入草稿失败',
            content: ''
        });

        $scope.uploadCallback = function(){

        }
        $scope.submit = function(isdraft) {
				 var data,
                	errors = [];
            	if (submiting) {
                	return;
            	}
            	$('.warns').removeClass('warns');
            	data = collectData();
            	data.recommend = data.recommend ? 'true' : 'false';
                $scope.showWarning = true;
                if (!$scope.title) {
                    submitFailedModal.data.title = '标题不能为空';
                    submitFailedModal.open();
                    return;
                }
                if ($scope.wordCount($scope.title) > $scope.titleNum) {
                    submitFailedModal.data.title = '标题不能超过' + $scope.titleNum + '字';
                    submitFailedModal.open();
                    return;
                }
                if ($scope.wordCount($scope.description) === 0 || $scope.wordCount($scope.description) > 128) {
                    // 摘要不能为空或超字数
                    $('.sider-container .description').addClass('warns');
                    errors.push('摘要不合要求');
                }
                if ($scope.keyword.length === 0) {
                    // 关键词不能为空
                    $('.sider-container .keyword .ui-taginput').addClass('warns');
                    errors.push('关键词不能为空');
                }
                if (!data.channel.length) {
                    $('.sider-container .channel').addClass('warns');
                    errors.push('未选择渠道');
                }
                if (!data.category.length) {
                    $('.category-row').addClass('warns');
                    errors.push('未选择栏目');
                    return ;
                }
                if ($.trim(data.content) === '') {
                    submitFailedModal.data.title = '内容不能为空';
                    submitFailedModal.open();
                    return;
                }
                if ($scope.uploading){
                    new Modal({type:'alert',title:'请等待文件上传完成'}).open();
                    return;
                }
                if (errors.length) {
                    return;
                }
                submitPostData(); // call submitFunc

            /**
             * 发送数据
             */
            function submitPostData()
            {
				data.contentid = $scope.content.id;
                data.siteid = cloud.siteId;
                submiting = true;
                $scope.onpublish = true;
                data.title = data.title || ' ';
                $http.post('/article/index/add', data).success(function(res) {
                    submiting = false;
                    $scope.onpublish = false;
                    if (isdraft) {
                        if (res.state) {
                            window.parent.cloud.transformOut();
                            setTimeout(function() {
                                $('[ng-controller="collect-admin-add-controller"]').trigger('init');
                            }, 1);
                        } else {
                            draftFailedModal.data.title = res.error;
                            draftFailedModal.open();
                        }
                        return;
                    }
                    if (res.state) {
                        data.status = 6;
                        $http.post('app/collect/Content/recontent', data).success(function(res) {});
                      //  $http.get('app/collect/Content/restatus'+'?'+'siteid='+cloud.siteId+'&contentid='+data.contentid+'&status=6');
                        // 云稿库记录log
                        if (parent && parent.cloud.superContent) {
                            $http.get('/supercontent/index/releaseHandle', {params: {
                                siteid: cloud.siteId,
                                customid: cloud.custom.id,
                                info_title: $scope.title,
                                info_appid: 1,
                                info_contentid: parent.cloud.superContent.id,
                                info_siteid: parent.cloud.superContent.siteid,
                                contentid: res.id,
                                superid: parent.cloud.superContent.superid
                            }});
                        }
                        $scope.publishState = 'published';
                        submitSuccessModal().open().then(function(cmd) {


                            if (cmd === 'view') {
                                window.open(res.url, "_blank");
                            }
                            // ------------------------- /
                            // 云稿库iframe调用检测
                            if (parent && parent.cloud.superContent) {
                                parent.location.reload();
                                return;
                            }
                            // ------------------------- /
                            $scope.editContent.state = false;
                        });
                    } else {
                        submitFailedModal.data.title = res.error;
                        submitFailedModal.open();
                    }
                });
            }
        };

        /* 返回 */
        backModal = new Modal({
            type: 'alert',
            title: ' ',
            content: ' ',
            buttons: [{
                name: "取消",
                cmd:false
            }, {
                name: "确定",
                cmd:true
            }]
        });
        $scope.goback = function() {
            var temp, tipstitle;
            //if ((!$scope.id && editor.getContent()) || ($scope.id && compareData(collectData(),dataBak).length > 0)) {
                if ($.trim($scope.title) === '') {
                    $scope.title = editor.getContent({
                        format: 'text'
                    }).substr(0, 24);
                }
                if ($scope.title.length > 20) {
                    tipstitle = $scope.title.substr(0, 20) + '...';
                } else {
                    tipstitle = $scope.title;
                }
                backModal.data.title = '是否退出编辑?';
                backModal.open().then(function(res){
                  if(res){
                    $scope.editContent.state = false;
                  }
                });
            /*}else {
                window.parent.cloud.transformOut();
                setTimeout(function() {
                    $('[ng-controller="collect-admin-add-controller"]').trigger('init');
                }, 1);
            }*/
        };
		$scope.title = $scope.content.title;
		$scope.category = [];
        var forMatCategory = $scope.content.category.split(',');
        if (forMatCategory) {
            for (var i in forMatCategory) {
                $scope.category.push(parseInt(forMatCategory[i]));
            }
        }

		$scope.channels = [
                        {
                            name: "pc",
                            alias: "PC",
                            value: true,
                            tips: '发布到PC站'
                        }, {
                            name: "phone",
                            alias: "手机",
                            value: true,
                            tips: '发布到客户端'
                        },
                        {
                            name: "wap",
                            alias: "WAP",
                            value: true,
                            tips: '发布到手机站'
                        }
                    ];
		$scope.thumb.id = $scope.content.thumbid;
		$scope.thumb.url = $scope.content.contentthumb;
		$scope.chooseThumbFromLibrary = true;
        $scope.source = $scope.content.source;
        $scope.sourceurl = $scope.content.sourceurl;

		if (editor) {
                    editor.setContent($scope.content.content);
                    if (editor.getContent()) {
                        editor.dom.doc.body.style.backgroundImage = '';
                    }
                }
        function getArticle(id) {
            $http.get(api.getContent, {
                params:{
                    id: id,
                    _: +new Date()
                }
            }).success(function(data) {
                data = data.data;
                data.category = data.category || [];
                dataBak = data;
                angular.forEach(data, function(val, key) {
                    $scope[key] = val;
                });
                $scope.author = ($scope.author || []).toString();
                $scope.keyword = ($scope.keyword || []).toString();
                $scope.sourceurl = $scope.sourceurl || '';
                $scope.$parent.sourceurl = $scope.sourceurl;
                $scope.recommend = ($scope.recommend == 1);
                $scope.chooseThumbFromLibrary = true;
                if (data.attachments) {
                    data.attachments.forEach(function (item) {
                        $scope.serverAttachments = $scope.serverAttachments || [];
                        $scope.serverAttachments.push({
                            name: item.alias,
                            size: item.size,
                            serverid: item.id,
                            serverurl: item.url
                        });
                    });
                }
                $scope.data={
                    enableAudit: $scope.enableAudit,
                    isImportant: $scope.isImportant
                };
                if($scope.auditinfoCache){
                    $scope.data.enableAudit = $scope.auditinfoCache.enableAudit ? ($scope.auditinfoCache.mode == 1 ? true : $scope.data.enableAudit) : false;
                }
                if($scope.description){
                    descripttionState = false;
                }
                if($scope.keyword){
                    keywordsState = false;
                }
                switchViewsData($scope.data.enableAudit ? 1 : 0);

                angular.forEach($scope.channels, function(val, i) {
                    if (data.channel && data.channel.indexOf(val.name) > -1) {
                        val.value = true;
                    } else {
                        val.value = false;
                    }
                });
                $scope.autofill = true;
                if (editor) {
                    editor.setContent($scope.content);
                    if (editor.getContent()) {
                        editor.dom.doc.body.style.backgroundImage = '';
                    }
                }
            });
        }
        function compareData(data1,data2){
            var filterKeys = ['id','isdraft','draftid','enableAudit'];
            var res = [];

            angular.forEach(data1,function(val,key){
                if(filterKeys.indexOf(key) > -1){
                    return;
                }

                if(!val || !data2[key]) {
                    if(val === '0'){val = 0;}
                    if(data2[key] === '0'){data2[key] = 0;}
                    if(data2[key] && data2[key].constructor===Array && data2[key].length === 0){
                        if(!!val){
                            res.push(key);
                        }
                        return;
                    }
                    if(!!val !== !!data2[key]){
                        res.push(key);
                    }
                    return;
                };

                if(key === 'thumb'){
                    if(val.id != data2[key].id){
                        res.push(key);
                    }
                    return;
                }

                if(val.constructor !== String && val.constructor !== Array){
                    if(val != data2[key]){
                        res.push(key);
                    }
                    return;
                }

                if(val.constructor === Array || data2[key].constructor === Array){
                    if(val.toString().split(',').sort().toString() !== data2[key].toString().split(',').sort().toString()){
                        res.push(key);
                    }
                    return;
                }

                if((val || '').toString() !== (data2[key] || '').toString()){
                    res.push(key);
                }
            });
            return res;
        }
        function collectData(){
            var data,channels = [];

            angular.forEach($scope.channels, function(item) {
                if (item.value) {
                    channels.push(item.name);
                }
            });

            //将页面中的数据格式化后保存到新的data中
            // 返回和提交数据都会用到
            data = {
                title: $scope.title || '',
                category: $scope.category,
                channel: channels,
                keyword: $scope.keyword,
                source: $scope.source || '',
                sourceurl: $scope.sourceurl || '',
                author: $scope.author,
                recommend: $scope.recommend,
                description: $scope.description,
                content: editor.getContent().replace(/src="a\//g, 'src="/a/'),
                enableAudit: !!$scope.data.enableAudit,
                isImportant: !!$scope.data.isImportant,
                attachments: $scope.data.attachments,
                enablecopyright: $scope.enablecopyright ? 1 : 0 //后台使用的是全小写
            };
			if (typeof $scope.published !== 'undefined')
			{
				data.published = $scope.published
			}
            if ($scope.thumb && $scope.thumb.id) {
                data.thumb = {
                    id: $scope.thumb.id,
                    url: $scope.thumb.url.replace(/^http:\/\/[^\/]*/, '')
                };
            }
			if ($scope.$parent.thumb && $scope.$parent.thumb.id) {
                data.thumb = {
                    id: $scope.$parent.thumb.id,
                    url: $scope.$parent.thumb.url.replace(/^http:\/\/[^\/]*/, '')
                };
            }

            return data;
        }

        setTimeout(function() {
            $("textarea.editor-title")[0].focus()
        }, 500);

        /* 自动计算 */
        nlpHanddler = {
            success: function(res) {
                res = res.data;
                if (!$scope.description || descripttionState) {
                    $scope.description = res.abstract && res.abstract.replace(/\n/gm, ' ');
                }
                if (!$scope.keyword || keywordsState) {
                    $scope.keyword = res.keyword && res.keyword.toString();
                }
                descriptionDom.trigger('update');
                var repeatContent = editor && editor.getContent({
                        format: 'text'
                    });
                detectionContentRepect($scope.title, repeatContent);
            },
            fail: function() {

            }
        }
        $('.keyword').on('keydown keyup click', 'input', function() {
            keywordsState = false;
        });

        function getthumb() {
            var imgs;
            if (!$scope.thumb.url) {
                imgs = $(editor.getDoc().images).toArray().filter(function(img) {
                    return img.getAttribute('data-mce-attachid') && ($scope.recommend ? img.width > 1280 * 860 : img.width > 250 && img.height > 100);
                });
                if (imgs.length) {
                    $scope.thumb.id = imgs[0].getAttribute('data-mce-attachid');
                    $scope.thumb.url = imgs[0].src;
                    $scope.chooseThumbFromLibrary = false;
                    $scope.$apply();
                }
            }
        }

        function setImgTags(data) {
            $http.post('/system/attachment/edit', data);
        }

        function wordNum(string) {
            //字数统计：中英文、英文单词
            var eNum = 0;
            string = string.replace(/(\w|\s)+/igm, function(a) {
                a = a.replace(/\s+/igm, ' ');
                eNum += $.trim(a).split(/\s/igm).length;
                return '';
            });
            return string.length + eNum;
        }

		function getdesciption(title, content) {
            return $http.post(autoabstractUrl, {
                    title: title,
                    content: content
                }).success(function(res) {
                    res.state && nlpHanddler.success(res);
                });
        }

        function createTooltip(ele) {
            var outterDom = document.createElement('div');
            var innerDom1 = document.createElement('div');
            var innerDom2 = document.createElement('div');
            var tipstxt = ele.attr('aria-label');
            if (!tipstxt) {
                return;
            }
            if ($('#mce-temp-tooltip').length) {
                $('#mce-temp-tooltip').remove();
            }
            outterDom.appendChild(innerDom1);
            outterDom.appendChild(innerDom2);
            $(outterDom).addClass('mce-widget mce-tooltip mce-tooltip-n');
            $(innerDom1).addClass('mce-tooltip-arrow');
            $(innerDom2).addClass('mce-tooltip-inner');
            $(outterDom).attr('id', 'mce-temp-tooltip');
            $(innerDom2).html(tipstxt);
            document.body.appendChild(outterDom);
            $(outterDom).css({
                top: ele.outerHeight() + ele.offset().top,
                left: (ele.outerWidth() / 2 - Math.ceil($(outterDom).outerWidth() / 2)) + ele.offset().left,
                'z-index': 131070
            });
        }

        $(document).on('nlpService', function() {
            var title, content;
            content = editor && editor.getContent({
                format: 'text'
            });
            title = $scope.title;
            getdesciption(title, content);
            return;
        });

        $scope.clearRepeatContent = function() {
            if ($scope.existsRepeatContent == false) return;
            setTimeout(function(){
                $('[ng-controller="collect-admin-add-controller"]').trigger('init');
                $("textarea.editor-title")[0].focus();
            }, 1);
            $scope.existsRepeatContent = false;
        }

        /**
         * Detection the Content exists Repeat
         */
        function detectionContentRepect(title, content) {

            /**
            * existsRepeatContent 存在重复内容
            * repeatLoading 正在检测重复
            * firstLoadingRepeat 防止触发多次检测，每次加载页面只检测一次
            */
            if ($scope.existsRepeatContent == true || $scope.repeatLoading == true || $scope.firstLoadingRepeat == true || query.id) return;
            $scope.existsRepeatContent = false;
            $scope.repeatLoading = true;
            $http.post('/article/index/checkRepeat', {title: title, content: content}).success(function(resp){
                if (resp.code === 0) {
                    $scope.existsRepeatContent = true;
                    $scope.existsRepeatDatas = resp.data;
                }
                $scope.repeatLoading = false;
                $scope.firstLoadingRepeat = true;
            });
        }

        $(".description-outter .placeholder").click(function() {
            $(this).parent().children('textarea').focus();
        });

        $scope.openThumbDialog = function() {
            if (!$scope.aca('attachment/thumblist', 'system')) {
                return Modal.alert('无此权限');
            }
            $(this).transform({
                reload: false,
                effect: 'fadeIn',
                url: '#/system/attachment/thumb?id=' + ($scope.thumb && $scope.thumb.id || '')
            });
            cloud.transformOut = function() {
                $(this).transform({
                    action: 'out'
                });
            }
        };
        window.onbeforeunload = function() {
            if ($scope.title && $scope.publishState !== 'published' && location.hash === '#/article/index/add') {
                return '内容尚未保存，您确认放弃发布吗？';
            }
        };
        $('.article-panel').find('.editor-text.editor-title').on({
            focus: function() {
                $('.article-panel').find('.editor-title-stat').show();
            },
            blur: function() {
                $('.article-panel').find('.editor-title-stat').hide();
            }
        });

        //复选框
        $scope.switchMode = function(newVal) {
            $scope.data.enableAudit = newVal;
            switchViewsData(newVal ? 1 : 0);
        };

        /**
         * 判断应用激活时间是否在一周内, 激活提示信息
         */
        (function() {
            var currentApp, firstTime, articleCookie;
            currentApp = cloud.app.filter(function(item) {
                return item.alias === 'article';
            })[0];
            if (!currentApp) {
                return;
            }
            firstTime = new Date(currentApp.firsttime * 1000);
            if (new Date() - firstTime < 604800000) { // = 86400000 * 7
                try {
                    articleCookie = commonService.toJson(window.cookie('app.article'));
                } catch (e) {
                    articleCookie = {};
                }
                $scope.showHelper = !articleCookie.hasShownAddHelper;
            }

            $http.get('/system/audit/getauditorstate').success(function(res) {
                $scope.auditinfoCache = res && res.data;
                if (/id=(\w+)/gi.test(window.location.toString())) {
                   getArticle(RegExp.$1);
                }

                if (!res.state || !res.data || !res.data.enableAudit) {
                    switchViewsData(0); //默认状态
                    return;
                }

                //获取审核配置信息
                if (res.data.mode == 1) {
                    //全部送审
                    $scope.data.enableAudit = true;
                    $scope.view.enableAuditCheckbox = false;
                } else {
                    //部分送审，显示选择复选框，复选框中的紧急应该是可以被禁用的
                    $scope.view.enableAuditCheckbox = true;
                }

                //修改提示信息文案
                switchViewsData($scope.data.enableAudit ? 1 : 0);

            });

            /**
             * 关闭帮助提示
             */
            $scope.closeHelper = function() {
                var articleCookie
                try {
                    articleCookie = commonService.toJson(window.cookie('app.article'));
                } catch (e) {
                    articleCookie = {};
                }
                articleCookie.hasShownAddHelper = true;
                $scope.showHelper = false;
                window.cookie('app.article', articleCookie, 7);
            }

            //初始化应用

            /*判断算是否是一篇新的内容*/
            if(query.id){
                $scope.isNewContent = false;
            }
			//  发布时间
			;(function () {
				$scope.maxPublished = new Date();
				$scope.showDatepicker = function(){
					$('.published-panel .date-input').trigger("click");
				}
				function dateToStamp(dateStr)
				{
					if (typeof dateStr !== 'string') return dateStr;
					var S = dateStr.replace(/[-]/g,"/");
					var D = new Date(S).getTime();
					return D;
				}
				function foramt_date(pattern,stamp)
				{
					var D = new Date(stamp);
					var T = {};
					T['YYYY'] = D.getFullYear();
					T['MM'] = D.getMonth() + 1 > 9 ? D.getMonth() + 1: "0"+(D.getMonth() + 1);
					T['DD'] = D.getDate()  > 9 ? D.getDate(): "0"+(D.getDate());
					T['hh'] = D.getHours()  > 9 ? D.getHours(): "0"+(D.getHours());
					T['mm'] = D.getMinutes()  > 9 ? D.getMinutes(): "0"+(D.getMinutes());
					T['ss'] = D.getSeconds()  > 9 ? D.getSeconds(): "0"+(D.getSeconds());
					for (var i in T)
					{
						pattern = pattern.replace(i,T[i])
					}
					return pattern;
				}
				$scope.$watch('_published',function (newVal,oldVal){
					var S = dateToStamp(newVal);
					if(S>0) $scope.published = foramt_date("YYYY-MM-DD hh:mm:ss",S);
				});
				$scope.$watch('published',function (newVal,oldVal){
					var S = dateToStamp(newVal);
					if(S>0) $scope._published = foramt_date("YYYY/MM/DD hh:mm",S)
				});
			})();

        }());
    }]);

}());
