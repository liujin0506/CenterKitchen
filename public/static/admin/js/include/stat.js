var remoteAppId_NTGJ = window.location.hash.replace(/^#\/|\/$/g, '');
define([
    "css!/" + remoteAppId_NTGJ + "/res/css/stat.css",
    "/assets/cloud/ui/scrollbar/scrollbar.js",
    "/assets/cloud/ui/highcharts/js/highcharts.js"
], function () {

    cloudModule.controllerProvider.register('collect-include-stat-controller', [
        '$scope', '$rootScope' ,'$element', 'Modal', '$http', "$q",
        function($scope, $rootScope, $element, Modal, $http, $q) {
            var api = {
                getTotal: "/collect/index/gettotal", // 总计接口
                getPublish: "/collect/index/getpublish", // 发发稿统计量
                getPublisher: "/collect/index/getbypublisher", // 发稿人
                getUpload: "/collect/index/getupload", // 素材统计
                getUploader: "/collect/index/getbyuploader" // 素材上传名单
            },
                initPage = 10,
                statDefaultParam = {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'area',
                        animation: false
                    },
                    title: {
                        text: '',
                        floating: true,
                        align: 'left',
                        x: 100,
                        y: 15,
                        style: {
                            color: 'gray',
                            fontSize: '12px'
                        }
                    },
                    tooltip: {
                        xDateFormat: '%Y-%m-%d',
                        shared: true
                    },
                    yAxis: {
                        title: {
                            text: '',
                            align: 'middle',
                            offset: 50,
                            y: 0,
                            enabled: false
                        },
                        lineWidth: 1,
                        offset: 20,
                        tickWidth: 1,
                        gridLineColor: '#efefef',
                        allowDecimals: false,
                    },
                    xAxis: {
                        useHTML: true,
                        labels: {
                            rotation: 45,
                            formatter: function () {
                                return Highcharts.dateFormat('%m.<strong>%d</strong>', new Date(this.value));
                            }
                        },
                        tickInterval: 2
                    },
                    series: [{
                        data: [],
                        name: '',
                        color: '#a7c27c',
                        marker: {
                            radius: 3
                        }
                    }, {
                        data: [],
                        name: '',
                        color: '#80a6ba',
                        marker: {
                            radius: 3
                        }
                    }]
                };

            $scope.totalData = {
                data: [],
                compare: null,
                get: null
            };
            $scope.publishStat = {
                time: [{name: '7', checked: true}, {name: '30', checked: false}],
                selectTime: "7",
                chart: {
                    xAxis: {
                        categories: [],
                        tickInterval: 1
                    },
                    series: [],
                    chart: {
                        width: $(window).width() - 215 - 80
                    }
                },
                get: null,
                loading: false
            };
            $scope.publishPerson = {
                time: [{name: '7', checked: true}, {name: '30', checked: false}],
                selectTime: "7",
                data: [],
                pages: {
                    total: 0,
                    perPage: initPage,
                    currentPage: 1,
                    maxSize: 5,
                    change: function(){
                        var page = arguments[1];

                        if(arguments.callee.oldPage && arguments.callee.oldPage == page){
                            return;
                        }
                        $scope.publishPerson.get && $scope.publishPerson.get($scope.publishPerson.selectTime, page);
                        arguments.callee.oldPage = page;
                    }
                },
                get: null,
                loading: false
            };
            $scope.mediaStat = {
                time: [{name: '7', checked: true}, {name: '30', checked: false}],
                selectTime: "7",
                chart: {
                    xAxis: {
                        categories: [],
                        tickInterval: 1
                    },
                    series: [],
                    chart: {
                        width: $(window).width() - 215 - 80
                    }
                },
                get: null,
                loading: false
            };
            $scope.mediaPerson = {
                time: [{name: '7', checked: true}, {name: '30', checked: false}],
                selectTime: "7",
                pages: {
                    total: 100,
                    perPage: initPage,
                    currentPage: 1,
                    maxSize: 5,
                    change: function(){
                        var page = arguments[1];

                        if(arguments.callee.oldPage && arguments.callee.oldPage == page){
                            return;
                        }
                        $scope.mediaPerson.get && $scope.mediaPerson.get($scope.mediaPerson.selectTime, page);
                        arguments.callee.oldPage = page;
                    }
                },
                data: [],
                get: null,
                loading: false
            };

            /**
             * 初始化统计页，在切换到统计页面时调用, 统计页面的入口
             */
            $rootScope.loadCollectStatData = function(){
                $q.when([
                    $scope.totalData.get(),
                    $scope.publishStat.get(),
                    $scope.publishPerson.get(),
                    $scope.mediaStat.get(),
                    $scope.mediaPerson.get()
                ]).then(function(data){

                });
            }

            /**
             * 切换数据天数
             * @param obj ：可选值（publishStat、publishPerson、mediaStat、publishPerson）
             * @param time ： 选择的天数
             */
            $scope.switchDate = function(obj, time){
                for(var i = 0; i < obj.time.length; i++){
                    if(obj.time[i].name == time){
                        obj.time[i].checked = true;
                        obj.selectTime = obj.time[i].name;
                    }else{
                        obj.time[i].checked = false;
                    }
                }
                obj.get && obj.get(obj.selectTime, obj.pages? (obj.pages.currentPage = 1): null, obj.pages? obj.pages.perPage: null);
            }

            /**
             * 导出数据表
             * @param source： 可选值publish/publisher/upload/uploader
             * @param date： 7/30
             */
            $scope.export = function(source, date){
                date = (date == 7) ? "seven" : "thirty";
                window.open('collect/index/export?source=' + source + '&date=' + date);

            }

            /**
             * 发稿统计量
             * @param date seven/thirty
             * @returns {*}
             */
            $scope.publishStat.get = function(date){
                var deferred = $q.defer(),
                    data = {
                        date: (date && date == 30)? "thirty": "seven"
                    };

                $scope.publishStat.loading = true;
                getData(api.getPublish, data, function(result){
                    if(result){
                        $scope.publishStat.chart.xAxis.categories = result.timeline;
                        $scope.publishStat.chart.series = result.data;
                        for(var i = 0; i < $scope.publishStat.chart.series.length; i++){
                            switch($scope.publishStat.chart.series[i].name){
                                case "article":
                                    $scope.publishStat.chart.series[i].name = "文章";
                                    break;
                                case "gallery":
                                    $scope.publishStat.chart.series[i].name = "图集";
                                    break;
                            }
                            $scope.publishStat.chart.series[i].y = $scope.publishStat.chart.series[i].data;
                        }
                        $scope.publishStat.chart.xAxis.tickInterval = (date == 30) ? 2 : 1;
                        $("#publish-stat").highcharts(updateStatParams($scope.publishStat.chart));
                        deferred.resolve(result);
                    }else{
                        deferred.reject(result);
                    }
                    $scope.publishStat.loading = false;
                });

                return deferred.promise;
            }

            /**
             * 发稿人
             * @param date seven/thirty,默认seven
             * @param page 第几页,默认1，从1开发
             * @param pagesize 分页大小  默认10
             * @returns {*}
             */
            $scope.publishPerson.get = function(date, page, pagesize){
                var deferred = $q.defer(),
                    data = {
                        date: (date && date == 30)? "thirty": "seven",
                        page: page || 1,
                        pagesize: pagesize || initPage
                    };

                $scope.publishPerson.loading = true;
                getData(api.getPublisher, data, function(result, total){
                    if(result){
                        $scope.publishPerson.data = result;
                        $scope.publishPerson.pages.perPage = pagesize || initPage;
                        $scope.publishPerson.pages.total = total;
                        deferred.resolve(result);
                    }else{
                        deferred.reject(result);
                    }
                    $scope.publishPerson.loading = false;
                });

                return deferred.promise;
            }

            /**
             * 素材统计
             * @param date seven/thirty
             * @returns {*}
             */
            $scope.mediaStat.get = function(date){
                var deferred = $q.defer(),
                    data = {
                        date: (date && date == 30)? "thirty": "seven"
                    };

                $scope.mediaStat.loading = true;
                getData(api.getUpload, data, function(result){
                    if(result){
                        $scope.mediaStat.chart.xAxis.categories = result.timeline;
                        $scope.mediaStat.chart.series = result.data;
                        for(var i = 0; i < $scope.mediaStat.chart.series.length; i++){
                            switch($scope.mediaStat.chart.series[i].name){
                                case "image":
                                    $scope.mediaStat.chart.series[i].name = "图片";
                                    break;
                                case "video":
                                    $scope.mediaStat.chart.series[i].name = "视频";
                                    break;
                            }
                            $scope.mediaStat.chart.series[i].y = $scope.mediaStat.chart.series[i].data;
                        }
                        $scope.mediaStat.chart.xAxis.tickInterval = (date == 30) ? 2 : 1;
                        $("#media-stat").highcharts(updateStatParams($scope.mediaStat.chart));
                        deferred.resolve(result);
                    }else{
                        deferred.reject(result);
                    }
                    $scope.mediaStat.loading = false;
                });

                return deferred.promise;
            }

            /**
             * 素材上传名单
             * @param date seven/thirty,默认seven
             * @param page 第几页,默认1，从1开发
             * @param pagesize 分页大小  默认10
             * @returns {*}
             */
            $scope.mediaPerson.get = function(date, page, pagesize){
                var deferred = $q.defer(),
                    data = {
                        date: (date && date == 30)? "thirty": "seven",
                        page: page || 1,
                        pagesize: pagesize || initPage
                    };

                $scope.mediaPerson.loading = true;
                getData(api.getUploader, data, function(result, total){
                    if(result){
                        $scope.mediaPerson.data = result;
                        $scope.mediaPerson.pages.perPage = pagesize || initPage;
                        $scope.mediaPerson.pages.total = total;
                        deferred.resolve(result);
                    }else{
                        deferred.reject(result);
                    }
                    $scope.mediaPerson.loading = false;
                });

                return deferred.promise;
            }

            /**
             * 获取总计信息
             */
            $scope.totalData.get = function(){
                var deferred = $q.defer();

                getData(api.getTotal, {}, function(result){
                    if(result){
                        $scope.totalData.data = result;
                        for(var i = 0; i < $scope.totalData.data.length; i++){
                            switch($scope.totalData.data[i].mode){
                                case "article":
                                    $scope.totalData.data[i].mode = "文章";
                                    break;
                                case "gallery":
                                    $scope.totalData.data[i].mode = "图集";
                                    break;
                                case "video":
                                    $scope.totalData.data[i].mode = "视频";
                                    break;
                                case "content":
                                    $scope.totalData.data[i].mode = "稿件";
                                    break;
                                case "attachment":
                                    $scope.totalData.data[i].mode = "素材";
                                    break;
                            }
                        }
                        deferred.resolve(result);
                    }else{
                        deferred.reject(result);
                    }
                });

                return deferred.promise;
            }

            /**
             * 对比每个模型的今天和昨天是上升、下降，还是不变
             * @param y 昨天的统计数据
             * @param t 今天的统计数据
             * @returns {number}
             */
            $scope.totalData.compare = function(y, t){
                var state = 0; // 持平

                t = Number(t);
                y = Number(y);
                if(t > y){
                    // 上升
                    state = 1;
                }else if(t < y){
                    // 下降
                    state = -1;
                }

                return state;
            }

            /**
             * get请求获取数据
             * @param url 请求url
             * @param data  请求
             * @param callback
             */
            function getData(url, data, callback){
                $http.get(url, {params: data}).success(function(res){
                    var result, total;

                    if(res && res.state){
                        result = res.data || false;
                        total = res.total || 0;
                    }else{
                        result = false;
                    }
                    callback && callback(result, total);
                }).error(function(){
                    callback && callback(false);
                });

            }

            /**
             * 更新统计默认参数
             * @param newValue ： 新的参数
             */
            function updateStatParams(newValue){
                var result = angular.copy(statDefaultParam);
                updateResult(newValue, result);
                return result;

                function updateResult(a, b){
                    for(var key in a){
                        if(typeof a[key] == "object" && b[key]){
                            updateResult(a[key], b[key]);
                        }else{
                            b[key] = a[key];
                        }
                    }
                }

            }

        }]);

});