(function() {
    'use strict';

    cloudModule.controllerProvider.register('politics-modal-setting-controller', [
        "$scope", "$http", "Modal","$timeout","$q","commonService",
        function ($scope, $http, Modal, $timeout,$q,commonService) {
            /**
             * 表单数据
             */
            $scope.baseinfo = {};
            $scope.smartInput = {};
            $scope.settingPage = {baseinfo : 1, deptMage : 0, deptAdd : 0, deptEdit : 0};
            $scope.banner = $scope.$parent.appResbaseDir+'images/default-banner.png';
            var noticeEditorDefer = $q.defer();
            var editHasModify = false;
            /**  基本信息设置 **/
            /**
             * 更新 banner
             */
            $scope.bannerUploader = function (res) {
                res = commonService.toJson(res);
                if(res.state)
                {
                    $scope.banner = res.data.banner;
                }
            };
            /**
             *  问政公告
             */
            //  获取公告数据
            $http.post($scope.appbaseurl+'setting/setting',{type:'select'}).success(function (res) {
                res = commonService.toJson(res);
                var notice = '';
                if (res.state)
                {
                    notice = res.data.notice.replace(/.*<body>(.*)<\/body>.*/img,"$1");
                    $scope.banner = res.data.banner;
                }
                $scope.baseinfo.notice = notice;
                noticeEditorDefer.promise.then(function(editor){
                    editor.setContent(notice);
                })
                
            });
            /**
             * 初始化编辑器
             */
            $scope.initEditor = function () {
                tinyMCE.remove('#editor');
                tinyMCE.activeEditor = null;
                $timeout(function () {
                    $("#editor").editor('report', {
                        autoresize_min_height: 270,
                        init_instance_callback: function (editor) {
                            var tinymceElement = $(editor.contentAreaContainer).parents('.mce-tinymce');
                            var tinymceContainer = tinymceElement.parents('.editor-container');
                            $(editor.contentAreaContainer).find('iframe')[0].contentDocument.body.style.paddingBottom = 0;
                            $(editor.contentAreaContainer).find('iframe')[0].contentDocument.body.style.minHeight = '100%';
                            editor
                                .on('focus', function() {
                                    tinymceElement.addClass('tinymce-focus');
                                })
                                .on('blur', function() {
                                    tinymceElement.removeClass('tinymce-focus');
                                })
                                .on('change',function(){
                                    editHasModify = true;
                                });
                            tinymceContainer.scroll(function (e) {
                                tinymceElement.find('.mce-toolbar-grp').css('top', tinymceContainer.scrollTop() - 5);
                            });

                            noticeEditorDefer.resolve(editor);
                        }
                    });
                }, 500);
            };
            /**
             * 基本信息设置完成 切换下一步
             */
            $scope.next = function () {
                if(editHasModify)
                {
                    var _value = tinyMCE.activeEditor.getContent();
                    var value = '<!DOCTYPE html><html><head><meta charset="utf-8"/>';
                    value +='<meta name="viewport" Content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>';
                    value +='<meta name="apple-mobile-web-app-status-bar-style" Content="black"  />';
                    value +='<meta name="apple-mobile-web-app-capable" Content="yes"></head><body>';
                    value += _value;
                    value += '</body></html>';
                    $scope.baseinfo.notice = value;
                
                    $http.post($scope.appbaseurl+'setting/setting', {
                        data : {notice:value.toString()},
                        type : 'up'
                    })
                    .success(function (res) {
                        //res = commonService.toJson(res);
                        $scope.settingPage.baseinfo=!($scope.settingPage.deptMage=1);
                    })
                    .error(function(){
                        $scope.settingPage.baseinfo=!($scope.settingPage.deptMage=1);
                    });
                }
                else{
                    $scope.settingPage.baseinfo=!($scope.settingPage.deptMage=1);
                }
            return 1;
            };
            
            /**
             * 设置-部门列表
             */
            $scope.deptAddInfo = {};
            $scope.deptEditInfo = {};
            $scope.deptlists = [];
            //  获取部门列表
            $http.get($scope.appbaseurl+'setting/grouplist').success(function (res) {
                res = commonService.toJson(res);
                if (res.state) 
                {
                    $scope.deptlists = res.data;
                }
            });
            //  部门添加 
            $scope.deptAddFields = [
                {id: 1, alias: 'name', name: '部门名称'},
                {id: 2, alias: 'email', name: '部门邮箱'},
                {id: 3, alias: 'contacts', name: '联系人'},
                {id: 4, alias: 'mobile', name: '手机'}
            ];
            $scope.deptAddFn = function(){
                $http.post(
                    $scope.appbaseurl+'setting/groupAdd',
                    $scope.deptAddInfo
                ).success(function (res) {
                    res = commonService.toJson(res);
                    if(!res.state)
                        Modal.error(res.error);
                    else
                    {
                        $scope.deptlists.push(res.data);
                        $scope.settingPage.deptAdd=!($scope.settingPage.deptMage=1);
                    }
                });
                return 1;
            };
            
            //  智能输入框获取焦点
            $scope.smartInputFocus = function(index)
            {
                var e = $("input[smart-input-index="+index+"]");
                setTimeout(function(){
                    e.focus();
                },300);
               return 1;
            };
            
            //  部门编辑
            $scope.deptEditFields = [
                {id: 5, alias: 'email', name: '部门邮箱'},
                {id: 6, alias: 'name', name: '部门名称'},
                {id: 7, alias: 'contacts', name: '联系人'},
                {id: 8, alias: 'mobile', name: '手机'}
            ];
            //  
            $scope.deptEditAct = function (id)
            {
                $.each($scope.deptlists,function(index,item){
                    if (id === item.id)
                    {
                        $scope.deptEditInfo = item;
                    }
                       
                });
                return 1;
            };
            
            $scope.deptEditFn = function() 
            {
                $http.post(
                    $scope.appbaseurl+'setting/groupEdit',
                    $scope.deptEditInfo
                ).success(function (res) {
                    res = commonService.toJson(res);
                    if(!res.state)
                        Modal.error(res.error);
                    else
                    {
                        for(var i in $scope.deptlists)
                        {
                            var item = $scope.deptlists[i];
                            if (res.data.id === item.id)
                                item = res.data;
                        }
                        $scope.settingPage.deptEdit=!($scope.settingPage.deptMage=1);
                    }
                });
                return 1;
            };
            //  
            function P(o)
            {
                console.log(o)
            }
        }// END
    ]);
}());