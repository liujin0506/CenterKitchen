(function(){
  cloudModule.provide.factory('commonService',function(){
    return {
      HttpId:{},
      getHttpId:function(type){
        var _number = this.HttpId[type];
        if(typeof _number === 'undefined'){
          _number = 0;
        }else{
          _number ++;
        }
        this.HttpId[type] = _number;
        return _number;
      },
      toJson:function(json){
        try{
            json = JSON.parse(json);
        }catch(e){
          console.log(e);
          json = {state:false,data:[]};
        }
        return json;
      }
    };
  });
})();