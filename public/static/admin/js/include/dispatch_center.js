define([
  "/assets/cloud/ui/datetimepicker/datetimepicker.js",
  "/assets/cloud/ui/dropmenupicker/dropmenupicker.js",

  "/" + remoteAppId_NTGJ + "/res/js/component/amap.js",
  "css!/" + remoteAppId_NTGJ + "/res/css/dispatch.css",
], function () {
  cloudModule.controllerProvider.register('app-collect-dispatch-center', [
    '$scope', "$timeout", "Modal", "$http", "$q", "$filter", "commonService",
    function ($scope, $timeout, Modal, $http, $q, $filter, commonService) {
      // url
      var url = {
        list_task:'dispatch/task?action=getlist',
        list_person:'dispatch/center?action=rlist',
        handle_task:'dispatch/task?action=operate',
        person_near:'dispatch/center?action=nearreporter&tid=',
        center_list:'dispatch/center',
        team_person:'dispatch/task?action=reporterlist',
        team_handle:'dispatch/task?action=addhelp',
        team_info:'dispatch/task?action=helpinfo&taskid='
      };
      for(var _u in url){
        url[_u] = $scope.appbaseurl + url[_u];
      }

      // 地图筛选 - 任务
      $scope.sortListTask = {
        from:'0',           //  1:媒体云爆料 2:外部接口爆料 3: 电话爆料
        task:{},            //  3:待处理 4:进行中 6:已完成
        time:'1',           //  今天1,7天,自定义(start：date,end：date)
        timeSelected:0,     //  组件的今天为0
        change:function(type,result){
          var _this = $scope.sortListTask;
          switch(type){
            case 'from':
              _this[type] = result.id;
              break;
            case 'task':
              _this[type][result] = !_this[type][result];
              break;
            case 'time':
              _this[type] = result;
              break;
            default:
          }
          $scope.amap.change();
        },
        getParams:function(){
          var _this = $scope.sortListTask;
          var _statusid = [];
          for(var _i in _this.task){
            if(_this.task[_i]){
              _statusid.push(_i);
            }
          }
          _statusid = _statusid.join(",");
          var _params = {
            time:_this.time,
            status:(_statusid === ''?'3,4,6':_statusid),
            from:_this.from
          };
          if(typeof _this.time === 'object'){
            _params.start = (new Date(_params.time[0]).getTime()/1000) - 8*60*60;
            _params.end = new Date(_params.time[1]).getTime()/1000 + 16*60*60;
            _params.time = 4;
          }
          return _params;
        },
        clear:function(){
          var _this = $scope.sortListTask;
          _this.from = '0';
          _this.task = {};
          _this.time = '1';
          _this.timeSelected = 0;
        }
      };

      // 地图筛选 - 人员
      $scope.sortListPerson = {
        person:{},
        change:function(type,result){
          var _this = $scope.sortListPerson;
          _this[type][result] = !_this[type][result];
          $scope.amap.change();
        },
        getParams:function(){
          var _this = $scope.sortListPerson;
          var _params = {};
          if((_this.person.hasTask && _this.person.noTask) ||
            (!_this.person.hasTask && !_this.person.noTask)){
            _params.hasmission = -1;
          }else{
            _params.hasmission = _this.person.hasTask ? '1' : '0';
          }
          return _params;
        },
        clear:function(){
          var _this = $scope.sortListPerson;
          _this.person = {};
        }
      };

      // 下拉data
      $scope.dropmenuSort = {
        from:$scope.dropmenuSort.from,
        // 任务状态
        status:[
          {id:'3,4,6',title:'全部'},
          {id:3,title:'待处理'},
          {id:4,title:'进行中'},
          {id:6,title:'已完成'},
        ],
        // 调度状态
        dispatch_status:[
          {id:'0',title:'全部'},
          {id:1,title:'待协同'},
          {id:2,title:'已协同'},
        ],
        // 协同状态
        // team_status:[
        //   {id:'0',title:'全部'},
        //   {id:1,title:'已确认'},
        //   {id:2,title:'协同中'},
        //   {id:3,title:'未到达'},
        //   {id:4,title:'已到达'},
        //   {id:5,title:'已完成'},
        // ],
        // 人员状态
        person:[
          {id:'-1',title:'全部'},
          {id:'1',title:'有任务'},
          {id:'0',title:'无任务'}
        ],
        // 人员排序
        person_sort:[
          {id:1,title:'降序'},
          {id:'tasknum',title:'升序'}
        ]
      };

      // 左侧列表切换
      $scope.dispatchListSlider = {
        timeid:null,
        state:false,
        timeCancel:function(){
          var _this = $scope.dispatchListSlider;
          $timeout.cancel(_this.timeid);
          _this.timeid = null;
        },
        change:function(init){
          var _this = $scope.dispatchListSlider;
          if(_this.timeid){
            _this.timeCancel();
          }
          _this.state = !_this.state;
          if(_this.state){
            _this.timeid = $timeout(function(){
              $scope.dispatchList.handle('task');
            },600);
          }else{
            $scope.nearTask.close();
            _this.timeid = $timeout(function(){
              $scope.dispatchList.clear();
              if(init){
                _this.change();
              }
            },600);
          }
        },
        clear:function(){
          var _this = $scope.dispatchListSlider;
          _this.state = false;
        }
      };

      // 左侧列表
      $scope.dispatchList = {
        index:-1,
        data:[],
        loadingOver:false,
        type:'task',    // task,person
        status:'3,4,6',
        dispatch_status:'0', // 调度状态
        team_status:'0', // 协同状态
        person:'-1',
        person_sort:'',
        successHandle:function(type){
          var _this = $scope.dispatchList;
          if(_this.index !== -1 && type === 'splice'){
            switch(_this.index.type){
              case 'personlist':
                $scope.nearTask.splice(_this.index.index,1);
                break;
              case 'tasklist':
                _this.data.splice(_this.index.index,1);
                break;
              default:
                _this.data.splice(_this.index.index,1);
            }
            _this.index = -1;
          }
        },
        select:function(type){
          var _this = $scope.dispatchList;
          _this.type = type;
          _this.status = '3,4,6';
          _this.dispatch_status = '0';
          _this.team_status = '0';
          _this.person = '-1';
          _this.person_sort = '';
          $scope.dispatchList.handle('init');
        },
        change:function(type,result){
          var _this = $scope.dispatchList;
          _this[type] = result.id;
          switch(type){
            case 'status':
              // 调度状态初始化
              if(result != 4){
                _this.dispatch_status = '0';
              }
              break;
            case 'dispatch_status':
              // 协同状态初始化
              if(result != 2){
                _this.team_status = '0';
              }
              break;
            case 'person_sort':
              if(result.id === 1){
                _this[type] = '';
              }
              return;
          }
          $scope.dispatchList.handle('init');
        },
        clear:function(){
          var _this = $scope.dispatchList;
          _this.data = [];
          _this.loadingOver = false;
          _this.type = 'task';
          _this.status = '3,4,6';
          _this.dispatch_status = '0';
          _this.person = '-1';
          _this.person_sort = '';
        },
        handle:function(type){
          var _this = $scope.dispatchList;
          var _type = _this.type.charAt(0).toUpperCase() + _this.type.substr(1);
          $scope['get' + _type].handle(type);
          $scope.nearTask.close();
        },
        getMore:function(){
          var _this = $scope.dispatchList;
          if(_this.loadingOver) return;
          if(_this.type === 'task'){
            $scope.getTask.handle('more');
          }
        }
      };

      // 左侧任务获取
      $scope.getTask = {
        params:{
          offset:0,
          pagesize:10,
        },
        handle:function(type){
          var _httpId = commonService.getHttpId('dispatchList');
          var _this = $scope.getTask;
          var _list = $scope.dispatchList;
          if(type === 'init'){
            _this.params.offset = 0;
            _list.loadingOver = false;
            _list.data = [];
          }
          if(type === 'more'){
            _this.params.offset += _list.data.length;
          }
          _this.params.status = $scope.dispatchList.status;
          _this.params.help = $scope.dispatchList.dispatch_status;
          // _this.params.help = $scope.dispatchList.team_status;
          $http.get(url.list_task,{params:_this.params}).success(function(res){
            if(_httpId !== commonService.HttpId.dispatchList) return;
            var _result = commonService.toJson(res);
            if(_result && _result.state && _result.data && _result.data.length){
              _list.data = _list.data.concat(_result.data);
              if(_result.data.length < _this.params.pagesize){
                _list.loadingOver = true;
              }
            }else{
              _list.loadingOver = true;
            }
          });
        }
      };

      // 人员获取
      $scope.getPerson = {
        handle:function(type){
          var _httpId = commonService.getHttpId('dispatchList');
          var _this = $scope.getPerson;
          var _list = $scope.dispatchList;
          var _params = {
            hasmission:$scope.dispatchList.person
          };
          if(type === 'init'){
            _list.data = [];
            _list.loadingOver = false;
          }
          $http.get(url.list_person,{params:_params}).success(function(res){
            if(_httpId !== commonService.HttpId.dispatchList) return;
            var _result = commonService.toJson(res);
            _list.data = _result.data || [];
            _list.loadingOver = true;
          });
        }
      };

      // 获取详情
      $scope.getContent = {
        loading:false,
        closeState:false,
        close:function(){
          $scope.modalList.close('infos');
          $scope.getContent.closeState = false;
          $scope.dispatchList.index = -1;
        },
        open:function(item,index,type,name){
          // var _time = 0;
          // if($scope.nearTask.newModalState){
          //   _time = 20;
          //   $scope.nearTask.close();
          // }
          // $timeout(function(){
            if(index !== '' && type){
              $scope.dispatchList.index = {type:type,index:index};
            }
            $scope.modalList.open('infos',function(){
              $scope.getContent.open(item,index,type,name);
            },$scope.getContent.close,name);
            $scope.goContent(item);
            $timeout(function(){
              $scope.getContent.closeState = true;
            },500);
          // });
        },
        submit:function(type,url,params){
            var _fun = function(){
                $("#infos").scrollTop(0);
                $scope.getContent.loading = true;
                $http.post(url,params).success(function(res){
                    $scope.getContent.loading = false;
                    var _result = commonService.toJson(res);
                    if(_result && _result.state){
                        $scope.getContent.close();
                    }else{
                        $scope.alertMsg("操作失败",res);
                    }
                });
            };
            if(type){
                new Modal({
                    title:'您确定' + type + '任务吗',
                    buttons:[
                        {name:'取消',cmd:false},
                        {name:'确定',cmd:true}
                    ]
                })
                .open().then(function(res){
                    res && _fun();
                });
            }else{
                _fun();
            }
        },
        taskHanle:function (type, status) {
            var _params = {
                taskid: $scope.content.id,
                status: status
            };
            var _url = url.handle_task;
            this.submit(type,_url,_params);
        }
      };

      // 取消任务
      $scope.taskCancel = {
        selectid:'',
        loading:false,
        text:'',
        data:'',
        reasonIndex:'',
        reason:$scope.setting.dataBak.reason||[],
        select:function(index){
          this.reasonIndex = index;
          this.data = index !== this.reason.length ? 
            this.reason[index].text : this.text;
        },
        input:function(){
          if(this.reasonIndex === this.reason.length){
            this.data = this.text;
          }
        },
        close:function(){
          var _this = $scope.taskCancel;
          $scope.modalList.close('cancel');
          _this.reasonIndex = '';
          _this.data = '';
          _this.text = '';
          $scope.dispatchList.index = -1;
        },
        open:function(itemid,index,type,name){
          var _this = $scope.taskCancel;
          if(index !== '' && type){
            $scope.dispatchList.index = {type:type,index:index};
          }
          _this.selectid = itemid;
          // var _time = 0;
          // if($scope.modalList.state.infos){
          //   $scope.getContent.close();
          //   _time = 50;
          // }
          // if($scope.nearTask.newModalState){
          //   $scope.nearTask.close();
          //   _time = 50;
          // }
          // $timeout(function(){
            $scope.modalList.open('cancel',function(){
              $scope.taskCancel.open(itemid,index,type,name);
            },_this.close,name);
          // },_time);
        },
        submit:function(){
          var _this = $scope.taskCancel;
          if(!_this.data) return;
          var _params = {
            taskid:_this.selectid,
            status:5,
            message:_this.data
          };
          _this.loading = true;
          $http.post(url.handle_task,_params).success(function(res){
            _this.loading = false;
            var _result = commonService.toJson(res);
            if(_result && _result.state){
              // $scope.alertMsg('取消成功');
              $scope.dispatchList.successHandle('splice');
              _this.close();
              $scope.amap.change();
            }else{
              $scope.alertMsg('取消失败',_result);
            }
          });
        }
      };

      // 分配协同
      $scope.teamHandle = {
        loading:false,
        state:false,
        timeid:null,
        roleid:-1,
        keyword:'',
        // 增加的协同人员列表
        selectItem:{},
        selectList:[],
        selectListId:[],
        list_all:[],
        list:[],
        dropmenuList:[],
        add:function(item){
          if(item.selected){
            return;
          }
          item.selected = true;
          this.selectList.push(item);
          this.selectListId.push(item.memberid);
          this.listClear();
        },
        remove:function(index){
          this.selectList[index].selected = false;
          this.selectList[index].selected.infos = '';
          this.selectList.splice(index,1);
          this.selectListId.splice(index,1);
        },
        listEvent:function(e){
          var _target = $(e.target);
          if(!_target.closest('.team-person-add-modal').length){
            $scope.$apply(function(){
              $scope.teamHandle.listClear();
            });
          }
        },
        listClear:function(){
          var _this = this;
          _this.state = false;
          $timeout(function(){
            _this.roleid = -1;
            _this.keyword = '';
            _this.list = angular.copy(_this.list_all);
          });
          document.removeEventListener('click',this.listEvent);
        },
        listChange:function(){
          this.state = true;
          $timeout(function(){
            document.addEventListener('click',$scope.teamHandle.listEvent);
          });
        },
        close:function(){
          var _this = $scope.teamHandle;
          _this.listClear();
          $scope.modalList.close('teamHandle');
          $timeout(function(){
            _this.selectItem = {};
            _this.selectList = [];
            _this.selectListId = [];
            _this.list = [];
            _this.list_all = [];
            _this.dropmenuList = [];
          },200);
        },
        open:function(id,index,name){
          var _this = $scope.teamHandle;
          $scope.modalList.open('teamHandle',function(){
            $scope.teamHandle.open(id,name);
          },_this.close,name);
          _this.selectItem.id = id;
          _this.selectItem.taskIndex = index;
          _this.init();
        },
        change:function(type,value){
          switch(type){
            case 'roleid':
              this[type] = value.id;
              break;
            default:
              this[type] = value;
          }
          this.dataFilter();
        },
        dataFilter:function(){
          var _this = this;
          if(_this.timeid){
            $timeout.cancel(_this.timeid);
            _this.timeid = null;
          }
          if(_this.roleid === -1 && _this.keyword === ''){
            _this.list = angular.copy(_this.list_all);
            return;
          }
          _this.timeid = $timeout(function(){
            var _list = [];
            angular.forEach(_this.list_all,function(item){
              if((_this.roleid === -1 || item.roleid == _this.roleid) && 
                (_this.keyword === '' || item.author.indexOf(_this.keyword) !== -1)){
                _list.push(item);
              }
            });
            _this.list = _list;
          },100);
        },
        submit:function(){
          var _this = this;
          if(!_this.selectList.length){
            $scope.alertMsg('未选择协同人员');
            return;
          }
          var _params = {
            taskid:_this.selectItem.id,
            helper:[]
          };
          var _obj;
          for(var i = 0;i < _this.selectList.length;i ++){
            _obj = _this.selectList[i];
            if(!_obj.infos){
              $scope.alertMsg('请填写任务描述',{
                error:'“' + _obj.author + '”' + '的任务描述为空'
              });
              return;
            }
            if(_obj.infos.length > 30){
              $scope.alertMsg('任务描述字数超出限制',{
                error:'请保持“' + _obj.author + '”' + '的任务描述字数在30字以内'
              });
              return;
            }
            _params.helper.push({
              helperid:_obj.memberid,
              helper:_obj.author,
              roleid:_obj.roleid,
              note:_obj.infos
            });
          }
          _this.loading = true;
          $http.post(url.team_handle,_params).success(function(res){
            _this.loading = false;
            res = commonService.toJson(res);
            if(res && res.state){
              // $scope.alertMsg('分配成功');
              var _index = _this.selectItem.taskIndex;
              if(_index || _index == '0'){
                // 耦合
                $scope.dispatchList.data[_index].help --;
              }
              _this.close();
            }else{
              $scope.alertMsg('分配失败',res);
            }
          }); 
        },
        get:function(type){
          var _this = this;
          _this.loading = true;
          $http.get(url.team_person + '&type=' + (type||2))
          .success(function(res){
            _this.loading = false;
            res = commonService.toJson(res);
            if(res && res.state){
              _this.list = res.reporter;
              _this.list_all = angular.copy(_this.list);
              _this.dropmenuHandle(res.role);
            }else{
              $scope.alertMsg('获取协同人员失败',res);
            }
          });
        },
        dropmenuHandle:function(data){
          var _this = this;
          var _list = [{
            id:-1,
            title:'全部岗位'
          }];
          angular.forEach(data,function(item){
            _list.push({
              id:item.id,
              title:item.name
            });
          });
          _this.dropmenuList = _list;
        },
        init:function(){
            var _this = this;
            _this.loading = true;
            $http.get(url.team_info + _this.selectItem.id)
            .success(function(res){
                res = commonService.toJson(res);
                if(res && res.state){
                    _this.get();
                    _this.selectItem.helpinfo = res.data;
                }else{
                    $scope.alertMsg('获取协同详情失败',res);
                }
            });
        }
      };
      
      // 分配任务
      $scope.personTasks = {
        item:{},
        selectIndex:-1,
        list:[],
        list_all:[],
        dropmenuList:[],
        roleid:-1,
        keyword:'',
        loading:false,
        open:function(item,index,type,name){
          var _this = $scope.personTasks;
          _this.item = item;
          if(index !== '' && type){
            $scope.dispatchList.index = {type:type,index:index};
          }
          $scope.modalList.open('person',function(){
            _this.open(item,index,type,name);
          },_this.close,name);
          $scope.personTasks.init();
        },
        close:function(){
          $scope.modalList.close('person');
          $scope.dispatchList.index = -1;
          var _this = this;
          $timeout(function(){
            _this.list = [];
            _this.roleid = -1;
            _this.keyword = '';
            _this.list_all = [];
            _this.dropmenuList = [];
            _this.selectIndex = -1;
          },200);
        },
        change:function(){
            $scope.teamHandle.change.call(this);
        },
        dropmenuHandle:function(data){
            $scope.teamHandle.dropmenuHandle.call(this,data);
        },
        dataFilter:function(){
            $scope.teamHandle.dataFilter.call(this);
        },
        init:function(){
            $scope.teamHandle.get.call(this,1);
        },
        submit:function(){
          var _this = $scope.personTasks;
          if(_this.selectIndex === -1){
            $scope.alertMsg('您未选择人员');
            return;
          }
          new Modal({
            title:'分配任务',
            content:'您确定将“' + 
              (_this.item.title.length > 11 ? (_this.item.title.substr(0,11) + '...') : _this.item.title) + 
              '“任务分配给“' + 
              _this.list[_this.selectIndex].author + '“吗？',
            buttons:[{name:'取消',cmd:false},{name:'确认',cmd:true}]
          }).open().then(function(res){
            if(res){
              var _params = {
                taskid:_this.item.id,
                status:1,
                reporterid:_this.list[_this.selectIndex].memberid
              };
              _this.loading = true;
              $http.post(url.handle_task,_params).success(function(res){
                _this.loading = false;
                var _result = commonService.toJson(res);
                if(_result && _result.state){
                  // $scope.alertMsg('分配成功');
                  $scope.dispatchList.successHandle('splice');
                  _this.close();
                  $scope.amap.change();
                }else{
                  $scope.alertMsg('分配失败',_result);
                }
              });
            }
          });
        }
      };

      // 附近任务
      $scope.nearTask = {
        loading:false,
        state:false,
        item:'',
        data:[],
        type:'self',
        loadingOver:false,
        newModalState:false,
        close:function(){
          var _this = $scope.nearTask;
          _this.state = false;
          if(_this.newModalState){
            $scope.modalList.state.common = false;
          }
          $timeout(function(){
            if(_this.newModalState){
              _this.newModalState = false;
            }
          },250);
        },
        getMore:function(){
          var _this = $scope.nearTask;
          if(_this.type === 'nearby') return;
          _this.selfGet();
        },
        init:function(item){
          var _this = $scope.nearTask;
          var _time = 0;
          if(_this.state){
            _time = 200;
            _this.close();
          }
          $timeout(function(){
            _this.item = item;
            _this.type = 'self';
            _this.data = [];
            _this.loadingOver = false;
            _this.selfGet(true);
            $timeout(function(){
              _this.state = true;
              if(_this.newModalState){
                $scope.modalList.state.common = true;
              }
            });
          },_time);
        },
        handle:function(listtype){
          var _this = $scope.nearTask;
          _this.type = listtype;
          _this.data = [];
          _this.loadingOver = false;
          _this[listtype + 'Get'](true);
        },
        selfGet:function(init){
          var _this = $scope.nearTask;
          if(_this.loadingOver) return;
          var _params = {
            action:'reporter',
            rid:_this.item.memberid,
            offset:init?0:_this.data.length,
            pagesize:15
          };
          $http.get(url.center_list,{params:_params}).success(function(res){
            var _result = commonService.toJson(res);
            if(_result && _result.state){
              _this.data = _this.data.concat(_result.data||[]);
            }
            if(_result.data.length < _params.pagesize){
              _this.loadingOver = true;
            }
          });
        },
        nearbyGet:function(){
          var _this = $scope.nearTask;
          var _params = {
            action:'neartask',
            limdistance:10000,
            orderby:1,
            status:3,
            reporterid:_this.item.id
          };
          $http.get(url.center_list,{params:_params}).success(function(res){
            _this.loadingOver = true;
            var _result = commonService.toJson(res);
            _this.data = _result.data || [];
          });
        },
        newModal:function(member){
          var _this = $scope.nearTask;
          // var _time = 0;
          // if(_this.state){
          //   _time = 50;
          //   _this.close();
          // }
          // $timeout(function(){
            _this.newModalState = true;
            _this.init(member);
            // !!!这里有深度耦合，解决一个复杂的同一模态框不同方式调用
            $scope.modalList.selected.nearTask = 'nearTask';
            $scope.modalList.currentType = 'nearTask';
            $scope.modalList.fn.nearTask = {
              open:function(){_this.newModal(member);},
              close:_this.close
            };
          // },_time);
        },
        splice:function(index){
          this.data.splice(index,1);
          // 取消任务-1 分配任务+1
          this.item.tasknum = this.type === 'self' ? 
          (+this.item.tasknum) - 1 : (+this.item.tasknum) + 1;
        },
        distrbTask:function(task,index){
          var _this = this;
          new Modal({
            title:'分配任务',
            content:'您确定将“' + 
              (task.title.length > 11 ? (task.title.substr(0,11) + '...') : task.title) + 
              '“任务分配给“' + 
              _this.item.author + '“吗？',
            buttons:[{name:'取消',cmd:false},{name:'确认',cmd:true}]
          }).open().then(function(res){
            if(res){
              var _params = {
                taskid:task.id,
                status:1,
                reporterid:_this.item.id
              };
              _this.loading = true;
              $http.post(url.handle_task,_params).success(function(res){
                _this.loading = false;
                var _result = commonService.toJson(res);
                if(_result && _result.state){
                  // $scope.alertMsg('分配成功');
                  _this.splice(index,1);
                  $scope.amap.change();
                }else{
                  $scope.alertMsg('分配失败',_result);
                }
              });
            }
          });
        }
      };

      // 消息处理
      $scope.messageList = {
        sort:[
          {id:'0',title:'全部'},
          {id:5,title:'取消任务'},
          {id:7,title:'拒绝任务'},
          {id:8,title:'协同任务'}
        ],
        data:[],
        loading:false,
        sortid:'0',
        keyword:'',
        state:false,
        loadingOver:false,
        selectIndex:{},
        deleteSuccess:function(){
          var _this = $scope.messageList;
          var _list = angular.copy(_this.data);
          var _index = [];
          for(var i in _this.selectIndex){
            _index.push(+i);
          }
          _index.sort().reverse();
          for(var j = 0;j < _index.length;j ++){
              _list.splice(_index[j],1);
          }
          _this.data = _list;
          _this.selectIndex = {};
        },
        delete:function(){
          var _this = $scope.messageList;
          var _id = [];
          for(var i in _this.selectIndex){
            _id.push(_this.data[i].id);
          }
          if(!_id.length){
            return $scope.alertMsg('未选择内容');
          }
          var _params = {
            action:'delmsg',
            id:_id.join(",")
          };
          _this.loading = true;
          $http.post(url.center_list,_params).success(function(res){
            _this.loading = false;
            var _result = commonService.toJson(res);
            if(_result && _result.state){
              // 删除成功
              _this.deleteSuccess();
            }else{
              $scope.alertMsg('删除失败',_result);
            }
          });
        },
        select:function(index){
          var _this = $scope.messageList;
          if(_this.selectIndex[index]){
            delete _this.selectIndex[index];
          }else{
            _this.selectIndex[index] = true;
          }
        },
        change:function(type,result){
          var _this = $scope.messageList;
          _this[type] = result.id;
          _this.handle('status');
        },
        getMore:function(){
          var _this = $scope.messageList;
          if(_this.loadingOver) return;
          _this.handle();
        },
        handle:function(type){
          var _this = $scope.messageList;
          if(type === 'init'){
            _this.keyword = '';
            _this.sortid = '0';
          }
          if(['search','init','status'].indexOf(type) !== -1){
            _this.selectIndex = {};
            _this.offset = '';
            _this.data = [];
            $(".center-list-message-list>ul")[0].scrollTop = 0; 
          }
          var _params = {
            action:'msg',
            status:_this.sortid,
            keyword:_this.keyword,
            offset:_this.data.length,
            pagesize:15,
          };
          _this.loadingOver = false;
          $http.get(url.center_list,{params:_params}).success(function(res){
            var _result = commonService.toJson(res);
            if(_result.data && _result.data.length){
              _this.data = _this.data.concat(_result.data||[]);
              if(_result.data.length < _params.pagesize){
                _this.loadingOver = true;
              }
            }else{
              _this.loadingOver = true;
            }
          });
        },
        closeHandle:function(e){
          var _this = $scope.messageList;
          if(!$(e.target)
          .closest('.message-toggle,.modal,.collect-modal,.collect-modal-wrap,.dispatch-center-modal-close').length){
            $scope.$apply(function(){
              _this.open();
            });
          }
        },
        open:function(){
          var _this = $scope.messageList;
          _this.state = !_this.state;
          if(_this.state){
            _this.handle('init');
            $timeout(function(){
              document.addEventListener('click',_this.closeHandle);
            });
          }else{
            document.removeEventListener('click',_this.closeHandle);
            $timeout(function(){
                $(".center-list-message-list>ul")[0].scrollTop = 0;
            },200);
          }
        },
        close:function(){
          var _this = $scope.messageList;
          _this.state = false;
        }
      };

      // 地图的筛选条件
      $scope.amap = {
        maxState:false,
        loading:true,
        personInfo:$scope.nearTask.newModal,
        taskContent:$scope.getContent.open,
        taskListChange:$scope.dispatchListSlider.change,
        search:{
          timeid:null,
          data:[],
          list:[],
          keyword:'',
          searchText:'',
          handle:function(){
            var _this = $scope.amap.search;
            var _data = [];
            if(_this.searchText){
              angular.forEach(_this.data,function(item){
                item.title = item.title || item.author;
                if(item.title && item.title.indexOf(_this.searchText) !== -1){
                  item.html = item.title
                    .replace(_this.searchText,'<span>' + _this.searchText + '</span>');
                  _data.push(item);
                }
              });
            }else{
              _this.list = [];
            }
            _this.list = _data;
          },
          change:function(){
            var _this = $scope.amap.search;
            if(_this.timeid){
              $timeout.cancel(_this.timeid);
              _this.timeid = null;
            }
            _this.timeid = $timeout(function(){
              _this.searchText = _this.keyword;
              _this.handle();
            },300);
          },
          select:function(item){
            var _this = $scope.amap.search;
            if(!_this.list.length && !item) return;
            $scope.amap.setCenter(item?[item]:_this.list);
          },
          clear:function(){
            var _this = $scope.amap.search;
            _this.keyword = '';
            _this.list = [];
            _this.searchText = '';
            if(_this.timeid){
              $timeout.cancel(_this.timeid);
              _this.timeid = null;
            }
          },
          init:function(arr){
            var _this = $scope.amap.search;
            _this.data = arr;
            if(_this.searchText) _this.handle();
          }
        },
        change:function(){
          $scope.amap.params = angular.extend(
            $scope.sortListTask.getParams(),
            $scope.sortListPerson.getParams()
          );
        }
      };
      $scope.amap.change();

      // 消息弹窗
      var localMsgCounts = 'msg_counts_' + cloud.custom.id;
      var localMsgHaveRead = 'msg_readlist' + cloud.custom.id;
      $scope.messageMin = {
        state:false,
        params:{action:'poller'},
        timeSpace:20000,
        viewTime:60000,
        viewId:null,
        turnId:null,
        turnState:false,
        timeid:null,
        data:{},
        newCount:+(localStorage.getItem(localMsgCounts) || 0),
        haveReadList:commonService.toJson(localStorage.getItem(localMsgHaveRead))||[],
        haveReadSet:function(id){
            this.haveReadList.push(id);
            localStorage.setItem(localMsgHaveRead,JSON.stringify(this.haveReadList));
        },
        close:function(){
          var _this = $scope.messageMin;
          _this.state = false;
          if(_this.data.data_bak.length && _this.turnId){
            $timeout.cancel(_this.turnId);
            _this.turnId = null;
            _this.state = false;
            _this.viewHandle();
          }
          if(_this.viewId){
            $timeout.cancel(_this.viewId);
            _this.viewId = null; 
          }
        },
        viewHandle:function(){
          var _this = $scope.messageMin;
          _this.data.data = _this.data.data_bak.splice(0,1)[0];
          if(_this.viewId){
            _this.state = false;
            $timeout.cancel(_this.viewId);
            _this.viewId = null;
          }
          $timeout(function(){
            _this.state = true;
            if(_this.data.data_bak.length){
              _this.turnId = $timeout(function(){
                _this.turnId = null;
                _this.state = false;
                _this.viewHandle();
              },3000);
            }else{
              _this.viewId = $timeout(function(){
                _this.state = false;
              },_this.viewTime);
              _this.handle();
            }
          },300);
        },
        initHandle:function(){
          var _this = $scope.messageMin;
          if($scope.dispatchSelect.navIndex !== 11) return;
          $http.get(url.center_list,{params:_this.params}).success(function(res){
            try{
              var _result = commonService.toJson(res);
              if(_result && _result.state){
                _this.newCount = 
                _result.count = Math.max(+_result.count + _this.newCount,0);
                localStorage.setItem(localMsgCounts,_result.count);
                _result.data_bak = angular.copy(_result.data);
                _this.data = _result;
                _this.viewHandle();
              }else{
                _this.data.count = _this.newCount;
                _this.handle();
              }
            }catch(e){
              _this.handle();
            }
          });
        },
        handle:function(){
          var _this = $scope.messageMin;
          _this.timeid = $timeout(function(){
            _this.initHandle();
          },_this.timeSpace);
        },
        init:function(){
          var _this = $scope.messageMin;
          if(_this.timeid){
            $timeout.cancel(_this.timeid);
            _this.timeid = null;
          }
          _this.initHandle();
        },
        countChange:function(init){
          var _this = $scope.messageMin;
          if(init){
            _this.data.count = 0;
            _this.newCount = 0;
          }else{
            _this.newCount = Math.max(_this.newCount--,0);
            _this.data.count = _this.newCount;
          }
          localStorage.setItem(localMsgCounts,_this.data.count);
        }
      };

      // init
      // sortListTask,sortListPerson,dispatchListSlider,dispatchList
      // getContent,taskCancel,personTasks,nearTask,messageList,messageMin
      // 排除(已经在窗口关闭时初始化):  taskCancel,getContent,personTasks,
      var init = {
        state:true,
        initName:"amap,messageMin,modalList",
        clearName:
          "sortListTask,sortListPerson,dispatchListSlider,dispatchList,amap," + 
          "nearTask,messageList,messageMin,modalList",
        do:function(type){
          var _this = init;
          var _arr = _this[type + 'Name'].split(",");
          var _type = type;
          if(type === 'clear'){
            if(_this.state) return;
            _this.state = true;
          }else{
            _this.state = false;
          }
          angular.forEach(_arr,function(item){
            if(_type === 'clear' && typeof $scope[item][_type] === 'undefined'){
              _type = 'close';
            }
            if(_type === 'init' && typeof $scope[item][_type] === 'undefined'){
              return;
            }
            $scope[item][_type]();
          });
        }
      };
      $scope.$watch('dispatchSelect.navIndex',function(){
        $timeout(function(){
          if($scope.dispatchSelect.navIndex === 11){
            init.do('init');
          }else{
            init.do('clear');
          }
        });
      });
    }
  ]);
}); 