(function() {
    'use strict';

    cloudModule.controllerProvider.register('politics-modal-reject-controller', [
        "$scope", "$http", "Modal", "commonService",
        function ($scope, $http, Modal, commonService) {
            /**
             * 确认
             */
            $scope.ok = function () {
                if (!$scope.message) return;
                $http.post($scope.$parent.appbaseurl+'Content/reject', {
                    id: $scope.$parent.content.id,
                    reject: $scope.message,
					status: 2
                }).success(function (res) {
                    res = commonService.toJson(res);
                    if (res.state) {
						$scope.$parent.reject = true;
						$scope.$parent.switchNav($scope.$parent.navIndex);
                        $scope.$close({state:true});
                    } else if(res.conflict){
                        Modal.alert({
                            title:'操作冲突',
                            content:res.error,
                            buttons : [{
                                name: '确定',
                                cmd: 'ok',
                                event: function() {
                                    $scope.$close({
                                        conflict: true,
                                        state:false,
                                        data: res.data
                                    });

                                    return true;
                                }
                            }]
                        });
                    }
                });
            };

            /**
             * 取消
             */
            $scope.cancel = function () {
                $scope.$close();
            }
        }
    ]);
}());
