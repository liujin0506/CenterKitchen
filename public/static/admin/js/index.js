var remoteAppId_NTGJ = window.location.hash.replace(/^#\/|\/$/g, '');

define([
    "css!/assets/cloud/common/style/Content-list.css",
    "css!/assets/cloud/common/style/form.css",
    "/assets/cloud/ui/switchapp/switchapp.js",
    "/assets/cloud/ui/userstate/userstate.js",
    "/assets/cloud/common/directive/contentfilter/contentfilter.js",
    "/assets/cloud/tools/filter.js",
    "/assets/cloud/ui/jplayer/jplayer.js",
    "/assets/cloud/ui/scrollbar/scrollbar.js",
    "/assets/cloud/ui/nicescrollbar/nicescrollbar.js",
    "/assets/cloud/ui/modal/modal.js",
    "/assets/cloud/common/service/contentselector/contentselector.js",
    "/assets/cloud/common/directive/formrow/formrow.js",
    "/assets/cloud/ui/uploader-html5/uploader.js",
    "/assets/cloud/editor/editor.js",
    "/assets/libraries/jquery.nicescroll/jquery.nicescroll.min.js",
    "/assets/cloud/ui/datepicker/datepicker.js",
    "/assets/cloud/ui/treemenu/index.js",
    "/assets/cloud/ui/taginput/taginput.js",
    "/assets/cloud/ui/suggest/suggest.js",
    "/assets/cloud/ui/urlinput/urlinput.js",
    "/assets/cloud/ui/modal/modal.js",
    "/assets/cloud/ui/transform/transform.js",
    "/assets/cloud/ui/copyright/copyright.js",
    "/assets/cloud/ui/attachment/attachment.js",
    "/assets/cloud/ui/zeroclipboard/clipboard.js",
    "/assets/cloud/ui/autobrinput/autobrinput.js",
    "/assets/cloud/ui/dropmenupicker/dropmenupicker.js",
    "/assets/cloud/ui/nicescrollbar/nicescrollbar.js",
    "/assets/module/poster/common/directive/switch/switch.js",

    "/" + remoteAppId_NTGJ + "/res/js/component/city_search/city_search.js",
    "/" + remoteAppId_NTGJ + "/res/js/component/common_service.js",
    "css!/" + remoteAppId_NTGJ + "/res/css/index.css",
    "css!/" + remoteAppId_NTGJ + "/res/css/dispatch.css",
    "css!/" + remoteAppId_NTGJ + "/res/css/modal.css",
    "css!/" + remoteAppId_NTGJ + "/res/css/add.css",
    "css!/" + remoteAppId_NTGJ + "/res/css/galleryadd.css",
    "/" + remoteAppId_NTGJ + "/res/js/filter.js",
    "/" + remoteAppId_NTGJ + "/res/js/add.js",
    "/" + remoteAppId_NTGJ + "/res/js/galleryadd.js",
    "/" + remoteAppId_NTGJ + "/res/js/modal/reject.js",
    "/" + remoteAppId_NTGJ + "/res/js/include/dispatch_center.js",
    "/" + remoteAppId_NTGJ + "/res/js/include/stat.js"
], function () {
    cloudModule.controllerProvider.register('app-collect-index', [
        '$rootScope', '$scope', "$timeout", "Modal", "$modal", "$sce", "$http", "DateFormat", "$q", "commonService",
        function ($rootScope, $scope, $timeout, Modal, $modal, $sce, $http, DateFormat, $q, commonService) {
            $scope.appbaseurl = "/" + remoteAppId_NTGJ + "/";
            $scope.appResbaseDir = $scope.appbaseurl + "res/";
            $scope.list = [];         //  内容列表
            $scope.content = {};      //  内容
            $scope.editContent = {
                state: false,
                content: {}
            };
            $scope.nav = {};
            $scope.loadingList = 0;
            $scope.loadingContent = 0;
            $scope.showPageurl = $scope.$root.siteUrl + $scope.appbaseurl + 'normal/show?id=';
            $scope.more = false;
            $scope.template = {
                welcome: "/" + remoteAppId_NTGJ + "/res/template/welcome.html",
                normal: "/" + remoteAppId_NTGJ + "/res/template/normal.html",
                add: "/" + remoteAppId_NTGJ + "/res/template/add.html",
                galleryadd: "/" + remoteAppId_NTGJ + "/res/template/galleryadd.html",
                commonModal: "/" + remoteAppId_NTGJ + "/res/template/modal/common.modal.html",
                dispatchCenter: "/" + remoteAppId_NTGJ + "/res/template/include/dispatch_center.html",
                dispatchSetting: "/" + remoteAppId_NTGJ + "/res/template/include/dispatch_setting.html",
            };
            $scope.statTemplate = "/" + remoteAppId_NTGJ + "/res/template/include/stat.html";
            $scope.tpl = $scope.template.normal;

            var navLock = false;
            var navQuery = {
                // 0: { my: 0, status: 3, offset: 0, pagesize: 15, modelid: 1 },
                1: { my: 1, status: 6, offset: 0, pagesize: 15 },
                // 下面综合到上面的大栏目下
                // 2: { my: 1, status: 3, offset: 0, pagesize: 15 },
                // 3: { my: 1, status: 2, offset: 0, pagesize: 15 },
                4: { my: 1, status: 4, offset: 0, pagesize: 15 },
                5: { status: 3, offset: 0, pagesize: 15 },
                6: { status: 6, offset: 0, pagesize: 15 },
                // 7: { status: 3, offset: 0, pagesize: 15, modelid: 2 },
                // 直播回顾
                8: { status: 0, offset: 0, pagesize: 15 },
                // 线索库
                9: { modules: 'clue', pagesize: 15, offset: 0, action: 'auditlist', status: 3, keyword: '', from: '0', orderby: 'desc' },
                // 任务列表
                10: { status: 3, pagesize: 15, offset: 0, action: 'getlist', keyword: '', from: '0', orderby: 'desc' },
                // 调度中心
                11: {},
                // 协同人员
                // 12: { my: 0, status: 0, offset: 0 },
                // 我的任务
                13: { modules: 'my', status: 0, pagesize: 15, offset: 0, action: 'mylist', keyword: '', from: '0', orderby: 'desc' },
                // 回收站
                16: { status: 0, offset: 0, pagesize: 15 },
                // 统计
                14: {},
                // 设置
                15: {},
            };
            var dispatchUrl = {
                tasklist: 'dispatch/task?action=getlist',
                taskedit: 'dispatch/task?action=edit',
                taskcontent: 'dispatch/task?action=Content&taskid=',
                taskhandle: 'dispatch/task?action=operate',
                clueedit: 'dispatch/clue?action=edit',
                cluehandle: 'dispatch/clue?action=audit',
                cluerecycle: 'dispatch/clue?action=recdetail&id=',
                settting: 'setting/general?action=',
                auth: 'auth/auth?userid=' + cloud.custom.id,
                teamState: 'dispatch/task?action=helpstatus',
                live_handle: 'Content/liveedit',
                taskClear: 'dispatch/task?action=clear'
            };
            for (var _u in dispatchUrl) {
                dispatchUrl[_u] = $scope.appbaseurl + dispatchUrl[_u];
            }

            // 获取设置和权限信息
            $scope.auth = {
                data: '',
                loading: true,
                default_get: function (auth, index, context) {
                    var _auth = auth.split(",");
                    var _index = index.split(",");
                    for (var i = 0; i < _auth.length; i++) {
                        if (context[_auth[i]].enable ||
                            (typeof context[_auth[i]].enable === 'undefined' && context[_auth[i]])
                        ) {
                            return _index[i];
                        }
                    }
                },
                handle: function (data) {
                    // 整理所有权限，上下级关系，很糟糕的权限
                    // 只有父级的时候不需要子集的权限了，注释掉
                    // 第一步整理所有的权限
                    var _params = {
                        // 左侧大的父级栏目
                        parents: {
                            navbar_top: true,
                            // 正在直播，直播回顾
                            live: data[1000710] || data[1000712],
                            // 线索库，任务列表（我的任务），调度中心
                            dispatch: data[1000715] || data[1000719] || data[1000717],
                            // 直播回顾，线索库（待审、已申），任务列表
                            recycle:
                            data[1000712] ||
                            (data[1000715] && data[1000716]) ||
                            data[1000719],
                            // 统计
                            stat: data[1000724],
                            // 常用设置，采集设置
                            setting: data[1000708] || data[1000709]
                        },
                        // 正在直播
                        live: {
                            // 左侧栏目
                            enable: data[1000710],
                            // 保存素材库
                            handle_save: data[1000711]
                        },
                        // 直播回顾
                        live_back: {
                            // 左侧栏目
                            enable: data[1000712],
                            // 保存素材库
                            handle_save: data[1000713],
                            handle_delete: data[1000714],
                        },
                        clue: {
                            enable: data[1000715] || data[1000716],
                            // 中间列表的头部导切换栏目
                            navbar_top: data[1000715] && data[1000716],
                            // 待审
                            audit_pending: data[1000715],
                            // 已审
                            audit_audited: data[1000716],
                        },
                        task: {
                            enable: data[1000719],
                            // 处理
                            handle: data[1000720],
                        },
                        dispatch_center: {
                            enable: data[1000717],
                            handle: data[1000718]
                        },
                        recycle: {
                            // 里面有三种数据
                            // 直播回顾，线索库，任务列表
                            enable:
                            (data[1000710] && data[1000712]) ||
                            (data[1000715] && data[1000716]) ||
                            data[1000719],
                            navbar_top:
                            ((data[1000710] && data[1000712]) && (data[1000715] && data[1000716])) ||
                            ((data[1000710] && data[1000712]) && data[1000719]) ||
                            (data[1000719] && (data[1000715] && data[1000716])),
                            handle_reset: data[1000721],
                            handle_clear: data[1000722],
                            handle_delete: data[1000723]
                        },
                        stat: {
                            enable: data[1000724],
                            handle_export: data[1000725]
                        },
                        setting: {
                            enable: data[1000708] || data[1000709],
                        }
                    };
                    // 第二部根据权限整理栏目的默认选项
                    // 左侧parent 
                    // 目前我的里面没有权限，有的话需要计算
                    _params.mine = true;
                    _params.parents.default_index = +this.default_get(
                        'live,live_back,clue,task,dispatch_center,mine',
                        '5,6,9,10,11,1',
                        _params
                    );
                    // 判断有navbar_top的默认选项
                    // 目前有clue,recycle两个
                    if (_params.clue.enable) {
                        _params.clue.default_index = +this.default_get(
                            'audit_pending,audit_audited',
                            '3,6',
                            _params.clue
                        );
                    }
                    // 回收站很特殊，有三种类型的判断
                    if (_params.recycle.enable) {
                        var _state = [
                            _params.live.enable && _params.live_back.enable,
                            _params.clue.audit_pending && _params.clue.audit_audited,
                            _params.task.enable
                        ];
                        var _index = ['recycle_live', 'recycle_clue', 'recycle_task'];
                        for (var i = 0; i < _state.length; i++) {
                            if (_state[i]) {
                                _params.recycle.default_index = _index[i];
                                break;
                            }
                        }
                    }
                    // 回收所有
                    $scope.auth = _params;
                },
                get: function () {
                    $http.get(dispatchUrl.auth).success(function (res) {
                        try {
                            var _result = commonService.toJson(res);
                            if (typeof _result === 'object') {
                                $timeout(function () {
                                    // 复杂的处理扔到后面处理
                                    $scope.auth.handle(_result);
                                    // 触发计算权限后的第一个默认栏目
                                    $scope.switchNav($scope.auth.parents.default_index);
                                });
                            } else {
                                alertMsg('获取配置失败', _result);
                                // 触发我的已发 这个必须是始终存在的
                                $scope.switchNav(1);
                            }
                        } catch (e) {
                            console.log(e);
                            // 触发我的已发 这个必须是始终存在的
                            $scope.switchNav(1);
                        }
                        $timeout(function () {
                            $scope.auth.loading = false;
                        });
                    });
                }
            };
            $scope.auth.get();
            $scope.setting = {
                loading: true,
                data: '',
                im: {},
                rongcount: 0,
                add: function () {
                    $scope.setting.data.reason.push({ text: '' });
                },
                delete: function (index) {
                    $scope.setting.data.reason.splice(index, 1);
                },
                get: function () {
                    $http.get(dispatchUrl.settting + 'get').success(function (res) {
                        $scope.setting.loading = false;
                        var _result = commonService.toJson(res);
                        if (_result && _result.state) {
                            var _text = [];
                            angular.forEach(_result.data.reason, function (item) {
                                _text.push({ text: item });
                            });
                            _result.data.reason = _text;
                            $scope.setting.data = _result.data;
                            $scope.setting.im = _result.data.rongcloud;
                            $scope.setting.rongcount = _result.data.rongcount;
                            $scope.setting.dataBak = angular.copy(_result.data);
                        } else {
                            alertMsg('获取配置失败', _result);
                        }
                    });
                },
                submit: function () {
                    var _this = $scope.setting;
                    var _text = [];
                    var _state = false;
                    if(/^(\d|-){7,}$/.test(_this.data.centerphone)){
                        var _len = _this.data.centerphone.replace(/-/g,'').length;
                        if(_len < 7 || _len > 12){
                            return alertMsg('请输入正确的电话号码');
                        }
                    }else{
                        return alertMsg('请输入正确的电话号码');
                    }
                    angular.forEach(_this.data.reason, function (item) {
                        if (!item.text ||
                            item.text.length < 5 ||
                            item.text.length > 20) {
                            _state = true;
                        }
                        _text.push(item.text);
                    });
                    if (_state) {
                        return alertMsg(
                            '输入有误',
                            {error:'输入理由请保持在5-20个字以内，不能为空'}
                        );
                    }
                    var _params = angular.copy(_this.data);
                    _params.reason = _text;
                    _this.loading = true;
                    $http.post(dispatchUrl.settting + 'set', _params).success(function (res) {
                        _this.loading = false;
                        var _result = commonService.toJson(res);
                        if (_result && _result.state) {
                            alertMsg('保存成功');
                            _this.dataBak = angular.copy(_this.data);
                        } else {
                            alertMsg('保存失败', _result);
                            _this.data = angular.copy(_this.dataBak);
                        }
                    });
                },
                setIm: function () {
                    var _params = angular.copy(this.im);
                    _params.state = this.im.state ? 1 : 0;
                    $http.post(dispatchUrl.settting + 'setim', {
                        rongcloud: _params
                    });
                }
            };
            $scope.setting.get();

            // 选中的对象
            $scope.dispatchSelect = {
                // 3:待审 recycle(6):回收站(需求更改了为已审) pending:待处理3 esc:已取消 5
                // close:已关闭2 liveback:直播回顾 progress:进行中4 success:已完成6
                // navbar:头部导航状态 
                // item:列表中的选中的项
                // keyword:关键词
                keyword: '',
                navbar: -1,
                navIndex: 0,
                selectedIndex: 0,
                bakData: {},
                item: { status: '' },
                changeHandle: function (type, isactive) {
                    var _this = $scope.dispatchSelect;
                    _this.navbar = type;
                    switch (isactive) {
                        case 1:
                            navQuery[isactive].status = type;
                            break;
                        case 9:
                            navQuery[isactive].status = type;
                            break;
                        case 10:
                            if (type === 3) {
                                type = '1,3';
                            }
                            navQuery[isactive].status = type;
                            break;
                        case 16:
                            var _type = '';
                            var _params = {};
                            // recycle_live clue task
                            switch (type) {
                                case 'recycle_live':
                                    _type = 6;
                                    _params = angular.copy(navQuery[_type]);
                                    _params.status = '7';
                                    break;
                                case 'recycle_clue':
                                    _type = 9;
                                    _params = angular.copy(navQuery[_type]);
                                    delete _params.status;
                                    _params.action = 'recover';
                                    break;
                                default:
                                    // task
                                    _type = 10;
                                    _params = angular.copy(navQuery[_type]);
                                    _params.status = '-2';
                            }
                            navQuery[isactive] = angular.copy(_params);
                            break;
                        default:
                    }
                },
                change: function (type, isactive) {
                    var _this = $scope.dispatchSelect;
                    _this.changeHandle(type, isactive || $scope.isactive);
                    $scope.sortList.reset(isactive || $scope.isactive);
                    $scope.switchNav(isactive || $scope.isactive, 1);
                },
                init: function (isactive) {
                    var _this = $scope.dispatchSelect;
                    switch (isactive) {
                        case 1:
                            _this.navbar = 6;
                            navQuery[isactive].status = _this.navbar;
                            break;
                        // 这里面要用过滤权限后的默认栏目 目前1:my 9:clue 16:recycle 里面有
                        case 9:
                            _this.navbar = $scope.auth.clue.default_index;
                            // _this.navbar = 3;
                            navQuery[isactive].status = _this.navbar;
                            break;
                        case 10:
                            // 增加一个状态 1：待确认
                            _this.navbar = 3;
                            navQuery[isactive].status = _this.navbar + ',1';
                            break;
                        // case 8:
                        //   _this.navbar = 'liveback';
                        //   navQuery[isactive].action = _this.navbar;
                        //   break;
                        case 16:
                            _this.navbar = $scope.auth.recycle.default_index;
                            // _this.navbar = 'recycle_live';
                            break;
                        default:
                            _this.navbar = -1;
                    }
                    if (_this.navbar !== -1) {
                        _this.changeHandle(_this.navbar, isactive);
                    }
                }
            };

            // 点击导航
            $scope.switchNav = function (index, notInit) {

                // 设置有修改是否未保存
                if ($scope.dispatchSelect.navIndex === 15 &&
                    !angular.equals($scope.setting.data, $scope.setting.dataBak)) {
                    new Modal({
                        title: '设置未保存',
                        content: '您修改了设置内容，但未保存，您确定离开吗?',
                        buttons: [{ name: '取消', cmd: false }, { name: '确定', cmd: true }]
                    }).open().then(function (res) {
                        if (res) {
                            $scope.setting.data = angular.copy($scope.setting.dataBak);
                            $scope.switchNav(index, notInit);
                        }
                    });
                    return;
                }

                if (index == 14 && $rootScope.loadCollectStatData) {
                    // 统计
                    $scope.isactive = index;
                    $scope.dispatchSelect.navIndex = index;
                    $scope.dispatchSelect.init(index);
                    $scope.sortList && $scope.sortList.reset(index);
                    $rootScope.loadCollectStatData();
                    return;
                }

                $scope.nav = { total: 0, list: [], _list: [], head: 0 };
                $scope.dispatchSelect.navIndex = index;
                $scope.loadingList = 1;
                $scope.content = {};
                navLock = false;
                $scope.more = false;
                if (!notInit) {
                    $scope.dispatchSelect.init(index);
                    $scope.sortList && $scope.sortList.reset(index);
                }
                var _httpId = commonService.getHttpId('getListData');
                getListData(index, function (res, index) {
                    if (_httpId !== commonService.HttpId.getListData) return;
                    $scope.loadingList = 0;
                    $scope.isactive = index;
                    $scope.nav.init = true;
                    $scope.nav.total = res.total;
                    //list_merge($scope.nav.list, res.data);
                    $scope.nav.list = res.data;
                    initList();
                    $scope.list.length ? $scope.goContent($scope.list[0]) : $scope.content = {};
                    $('.Content-list').scrollTop(0);
                    $('.detail-panel-outter').scrollTop(0);
                });
            };

            // 加载更多
            $scope.loadMore = function () {
                if (navLock || navQuery[$scope.isactive].offset >= $scope.nav.total) return;
                navLock = true;
                $scope.loadingList = 1;
                $scope.more = true;
                getListData($scope.isactive, function (res, index) {
                    if (res.state) {
                        // var nav = $scope.navs[index], list = nav.list;
                        // list_merge(list, res.data);
                        list_merge($scope.nav.list, res.data);
                        initList();
                    }

                    $scope.loadingList = 0;
                    navLock = false;
                });
            };

            // 刷新数据
            $scope.refresh = function (index) {

                if (index == $scope.isactive) return;

                var _list = $scope.nav._list;

                listRemove($scope.nav.list, $scope.content.id);
                listRemove($scope.list, $scope.content.id);
                $scope.nav.total--;
                $scope.nav.total++;
                navQuery[$scope.isactive].offset--;
                _list.push($scope.content);
                $scope.content = $scope.nav.list[$scope.nav.head];
                $scope.editContent.content = angular.copy($scope.content);

                if ($scope.list.length < navQuery[$scope.isactive].pagesize);
                {
                    $scope.loadMore();
                }
            };

            // 获取详情的处理
            $scope.contentHandle = function (item) {
                $scope.loadingContent = 1;
                $scope.content = angular.copy(item);
                $scope.content.videoHtml = $sce.trustAsHtml($scope.content.video);
                if (item.modelid == 2) {
                    $scope.content.content = commonService.toJson($scope.content.content);
                    $scope.content.formatedPicture = (function () {
                        var col = calcContentWidth();
                        var res = [];

                        $scope.content.content.forEach(function (item, i) {
                            res[i % col] = res[i % col] || [];
                            res[i % col].push(item);
                        });
                        return res;
                    }());
                }
                $scope.nav.head = $scope.isactive;
                $scope.loadingContent = false;
                $scope.editContent.content = angular.copy($scope.content);

                $('.detail-panel-outter').scrollTop(0);
                return true;
            };
            $scope.goContent = function (item, index) {
                var _httpId = commonService.getHttpId('goContent');
                $scope.dispatchSelect.selectedIndex = index || 0;
                $scope.dispatchSelect.item = item;
                switch ($scope.isactive) {
                    case 9:
                    case 10:
                    case 11:
                    case 13:
                    case 16:
                        // 回收站的直播回顾处理
                        if ($scope.isactive === 16 && $scope.dispatchSelect.navbar === 'recycle_live') {
                            return $scope.contentHandle(item);
                        }
                        // 线索库
                        $scope.clueEdit.editState = false;
                        var _url = dispatchUrl.taskcontent;
                        if ($scope.dispatchSelect.navbar === 'recycle_clue') {
                            _url = dispatchUrl.cluerecycle;
                        }
                        $scope.dispatchContent.get(_url, item.taskid || item.id, function (res) {
                            if (_httpId !== commonService.HttpId.goContent) {
                                // 回调多了 这里临时处理一下 加个loading
                                $scope.loadingContent = true;
                                return;
                            }
                            if (!res.data.clue) {
                                res.data.clue = res.data;
                            }
                            $scope.dispatchSelect.bakData = angular.copy(res.data);
                            $scope.content = res.data;
                        });
                        break;
                    case 12:
                        // 协同人员(修改了)
                        break;
                    default:
                        $scope.contentHandle(item);
                }
            };

            // 旧代码
            $scope.edit = function (item) {
                if(filterModelid(item.modelid)) return;
                if (item instanceof Object) {
                    $scope.editContent.content = item;
                }
                if ([0, 10, 11, 13].indexOf($scope.isactive) !== -1) {
                    $scope.editContent.tpl = $scope.template.add;
                }
                if ($scope.isactive === 7) {
                    $scope.editContent.tpl = $scope.template.galleryadd;
                }
                $scope.editContent.state = true;
            };
            // 预览
            $scope.stateTo = function(item) {
                if(filterModelid(item.modelid)) return;
                window.open(item.url);
            };
            $scope.auditpass = function () {
                var modal = new Modal({
                    type: 'alert',
                    title: '通过稿件',
                    content: '您确定要通过这篇稿件吗？',
                    buttons: [{
                        name: '取消',
                        cmd: 'canel'
                    }, {
                        name: '确定',
                        cmd: 'ok',
                        event: function () {
                            var param = {};
                            param.contentid = $scope.content.id;
                            param.siteid = $scope.content.siteid;
                            param.status = 6;
                            $http.post(
                                $scope.appbaseurl + 'Content/audit'
                                , param
                            ).success(function (res) {
                                $scope.isactive = 0;
                                res = commonService.toJson(res);
                                if (res.state) {
                                    navQuery[0].offset += res.data.length;
                                }
                                $scope.switchNav(0);
                            }).error(function () {
                                Modal.error('请求失败');
                            });
                        }
                    }]
                });
                modal.open().then(function (res) {
                });
            };
            $scope.auditreject = function () {
                var modal = $modal.open({
                    backdrop: "static",
                    templateUrl: $scope.appResbaseDir + "template/reject.html",
                    windowClass: "politics-modal",
                    scope: $scope,
                    controller: "politics-modal-reject-controller"
                });
                modal.result.then(function (res) {
                    if (res.state) {
                        $scope.content.status = 4;
                        //$scope.refresh(4);
                        $scope.content.audit = 1;
                    }
                    else if (res.conflict) {
                        //$scope.Content.groupid = res.data.groupid;
                        //$scope.Content.anonymity = res.data.anonymity;
                        //$scope.Content.status = res.data.status;
                        //$scope.Content.audit = res.data.audit;
                        //$scope.Content.secede = res.data.secede;
                        //var _indexs = {1:1, 2:2, 3:3, 4:4, 5:1};
                        //$scope.refresh(_indexs[res.data.status]);
                    }

                });
            };
            /* 保存到素材库 先不要
            $scope.addlive = function () {
              new Modal({
                type: 'alert',
                title: '保存素材库',
                Content: '您确定要将这篇稿件保存到素材库吗？',
                buttons: [{
                  name: '取消',
                  cmd: 'canel'
                }, {
                  name: '确定',
                  cmd: 'ok',
                  event: function () {
                    var param = {};
                    param.liveid = $scope.Content.id;
                    param.siteid = $scope.Content.siteid;
                    param.status = 6;
                    $scope.loadingList = true;
                    $http.post(
                      $scope.appbaseurl + 'Content/livemedia'
                      , param
                    ).success(function (res) {
                      $scope.loadingList = false;
                      var _res = commonService.toJson(res);
                      if(res && res.state){
                        alertMsg('保存成功');
                      }else{
                        alertMsg('保存失败',res);
                      }
                    });
                  }
                }]
              }).open();
            };
            */
            // 旧代码 end

            // 获取content
            $scope.dispatchContent = {
                data: '',
                get: function (url, taskid, callback) {
                    // 线索详情在列表里面
                    if ($scope.isactive === 9) {
                        callback({ data: angular.copy($scope.list[$scope.dispatchSelect.selectedIndex]) });
                        return;
                    }
                    $scope.loadingContent = true;
                    $http.get(url + taskid).then(function (res) {
                        $scope.loadingContent = false;
                        var _result = commonService.toJson(res.data);
                        if (_result && _result.state) {
                            callback(_result);
                        } else {
                            alertMsg('获取内容失败', _result);
                            callback([]);
                        }
                    });
                }
            };
            // 下载菜单（data/fun）
            $scope.dropmenuSort = {
                // 我的任务状态筛选
                status: [
                    { id: '0', title: '全部' },
                    { id: 4, title: '进行中' },
                    { id: 6, title: '已完成' },
                    { id: 5, title: '已取消' },
                    { id: 2, title: '已关闭' },
                ],
                status_pending: [
                    { id: '3,1', title: '全部' },
                    { id: '3', title: '待处理' },
                    { id: '1', title: '待确认' },
                ],
                sort: [
                    { id: 'published', title: '日期' },
                ],
                // 1:媒体云爆料 2:外部接口爆料 3: 电话爆料
                from: [
                    { id: '0', title: '全部' },
                    { id: 1, title: '报料' },
                    // {id:3,title:'电话报料'},
                    // {id:2,title:'外部接口报料'},
                ]
            };
            $scope.sortList = {
                fromid: '0',
                sortid: '',
                status: '0',
                status_pending: '3,1',
                reset: function (isactive) {
                    var _this = $scope.sortList;
                    _this.fromid = '';
                    _this.sortid = '';
                    _this.status = '';
                    _this.status_pending = '';
                    $scope.dispatchSelect.keyword = '';
                    if ([8, 9, 10, 13].indexOf(isactive) !== -1) {
                        navQuery[isactive].from = '0';
                        navQuery[isactive].orderby = 'desc';
                        navQuery[isactive].keyword = '';
                    }
                },
                change: function (type, result) {
                    var _this = $scope.sortList;
                    switch (type) {
                        case 'sort':
                            // 排序
                            navQuery[$scope.isactive].orderby = result.sort;
                            break;
                        case 'status_pending':
                            navQuery[$scope.isactive].status = result.id;
                            break;
                        default:
                            navQuery[$scope.isactive][type] = result.id;
                    }
                    $scope.switchNav($scope.isactive, 1);
                }
            };
            // 列表的编辑
            $scope.dispatchEdit = {
                listSplice: function (type) {
                    if(type === '清空'){
                        $scope.list = [];
                        return;
                    }
                    var _index = $scope.dispatchSelect.selectedIndex;
                    $scope.list.splice(_index, 1);
                    $scope.nav.total = Math.max(+$scope.nav.total - 1, 0);
                    if ($scope.list.length) {
                        if ($scope.list[_index]) {
                            $scope.goContent($scope.list[_index], _index);
                        } else {
                            $scope.goContent($scope.list[_index - 1], _index - 1);
                        }
                    } else {
                        $scope.switchNav($scope.isactive, 1);
                    }
                },
                handle: function (type, url, _params) {
                    if ($scope.loadingList) return;
                    new Modal({
                        title: '您确定' + type + '吗',
                        buttons: [{ name: '取消', cmd: false }, { name: '确认', cmd: true }]
                    }).open().then(function (res) {
                        if (res) {
                            $scope.loadingList = true;
                            $http.post(url, _params).success(function (res) {
                                $scope.loadingList = false;
                                var _result = commonService.toJson(res);
                                if (_result && _result.state) {
                                    $scope.dispatchEdit.listSplice(type);
                                } else {
                                    alertMsg(type + '失败', _result);
                                }
                            });
                        }
                    });
                },
                reject: {
                    text: '',
                    maxlen: 200,
                    loading: false,
                    title: '驳回理由',
                    open: function (title) {
                        var _this = $scope.dispatchEdit.reject;
                        _this.title = title;
                        $scope.modalList.open('reject');
                    },
                    close: function () {
                        var _this = $scope.dispatchEdit.reject;
                        _this.text = '';
                        $scope.modalList.close('reject');
                    },
                    getParams: function () {
                        var _params, _url;
                        var _this = $scope.dispatchEdit.reject;
                        switch ($scope.isactive) {
                            case 9:
                                // 线索驳回理由
                                _url = dispatchUrl.cluehandle;
                                _params = {
                                    id: $scope.content.clueid || $scope.content.id,
                                    status: 2,
                                    message: _this.text
                                };
                                break;
                            case 10:
                                // 任务取消理由
                                _url = dispatchUrl.taskhandle;
                                _params = {
                                    taskid: $scope.content.id,
                                    status: 5,
                                    message: _this.text
                                };
                                break;
                            default:
                        }
                        return { params: _params, url: _url };
                    },
                    submit: function () {
                        var _this = $scope.dispatchEdit.reject;
                        if (_this.text && _this.text.length) {
                            var _params = _this.getParams();
                            _url = _params.url;
                            _params = _params.params;
                            _this.loading = true;
                            $http.post(_url, _params).success(function (res) {
                                _this.loading = false;
                                var _result = commonService.toJson(res);
                                if (_result && _result.state) {
                                    _this.close();
                                    $scope.dispatchEdit.listSplice();
                                } else {
                                    alertMsg('操作失败', _result);
                                }
                            });
                        }
                    }
                }
            };
            // taskList
            $scope.taskList = {
                handle: function (type, status) {
                    var _params = {
                        taskid: $scope.content.id,
                        status: status
                    };
                    $scope.dispatchEdit.handle(type, dispatchUrl.taskhandle, _params);
                },
            };
            // taskedit
            $scope.taskEdit = {
                handle: function (url, params, callback) {
                    $http.post(url, params).success(function (res) {
                        var _result = commonService.toJson(res);
                        if (_result && _result.state) {
                            callback(true);
                        } else {
                            callback(false);
                        }
                    });
                },
                cancel: function () {
                    var _this = $scope.taskEdit;
                    $scope.content = angular.copy($scope.dispatchSelect.bakData);
                    $scope.clueEdit.editState = false;
                },
                submit: function () {
                    var _this = $scope.taskEdit;
                    if (angular.equals($scope.content, $scope.dispatchSelect.bakData)) {
                        _this.cancel();
                    } else {
                        if ($scope.content.title === '') {
                            return alertMsg('标题不能为空');
                        }
                        if ($scope.content.clue.desc === '') {
                            return alertMsg('详情不能为空');
                        }
                        $scope.loadingContent = true;
                        var _task = $q.defer();
                        var _clue = $q.defer();
                        var _task_params = {
                            taskid: $scope.content.id,
                            title: $scope.content.title
                        };
                        var _clue_params = angular.copy($scope.content.clue);
                        delete _clue_params.status;
                        _this.handle(dispatchUrl.taskedit, _task_params, function (res) {
                            _task.resolve(res);
                        });
                        _this.handle(dispatchUrl.clueedit, _clue_params, function (res) {
                            _clue.resolve(res);
                        });
                        $q.all([_task.promise, _clue.promise]).then(function (res) {
                            $scope.loadingContent = false;
                            if (res[0] && res[1]) {
                                alertMsg('修改成功');
                                // 更新列表中的title
                                angular.extend($scope.list[$scope.dispatchSelect.selectedIndex], {
                                    title: $scope.content.title
                                });
                                // 更新备份数据
                                $scope.dispatchSelect.bakData = angular.copy($scope.content);
                                $scope.clueEdit.editState = false;
                            } else {
                                alertMsg('修改失败');
                                _this.cancel();
                            }
                        });
                    }
                }
            };
            // clueEdit
            $scope.clueEdit = {
                editState: false,
                cancel: function () {
                    var _this = $scope.clueEdit;
                    $scope.content.clue = angular.copy($scope.dispatchSelect.bakData.clue);
                    _this.editState = false;
                },
                submit: function () {
                    var _this = $scope.clueEdit;
                    if (angular.equals($scope.content.clue, $scope.dispatchSelect.bakData.clue)) {
                        _this.cancel();
                    } else {
                        if ($scope.content.clue.title === '') {
                            return alertMsg('标题不能为空');
                        }
                        if ($scope.content.clue.desc === '') {
                            return alertMsg('线索详情不能为空');
                        }
                        var _params = {
                            id: $scope.content.clue.id,
                            title: $scope.content.clue.title,
                            desc: $scope.content.clue.desc
                        };
                        $scope.loadingContent = true;
                        $http.post(dispatchUrl.clueedit, _params).success(function (res) {
                            $scope.loadingContent = false;
                            var _result = commonService.toJson(res);
                            if (_result && _result.state) {
                                alertMsg('修改成功');
                                // 更新列表中的title
                                angular.extend($scope.list[$scope.dispatchSelect.selectedIndex], _params);
                                $scope.dispatchSelect.bakData.clue = angular.copy($scope.content.clue);
                                _this.editState = false;
                            } else {
                                alertMsg('修改失败', _result);
                                _this.cancel();
                            }
                        });
                    }
                }
            };
            // clueList
            $scope.clueList = {
                handle: function (type, status) {
                    var _params = {
                        id: $scope.content.clueid || $scope.content.id,
                        status: status
                    };
                    $scope.dispatchEdit.handle(type, dispatchUrl.cluehandle, _params);
                }
            };
            // 协同
            $scope.teamState = {
                loading: false,
                data: [
                    {
                        title: '已完成',
                        time: 0,
                        author: 1,
                        msg: '已到达',
                        icon: ''
                    },
                    // {
                    //   title:'已确认',
                    //   time:0,
                    //   author:1,
                    //   msg:'已确认',
                    //   icon:''
                    // },
                    {
                        title: '已分配',
                        time: 0,
                        author: 1,
                        msg: '电话',
                        phone: 1,
                        icon: 'allocation-state'
                    },
                    {
                        title: '协同失败',
                        time: 0,
                        author: 1,
                        msg: '协同失败',
                        icon: 'state-false'
                    }
                    // {
                    //   title:'已提交申请',
                    //   time:0,
                    //   msg:'等待调度中心确认',
                    //   icon:'audit-state'
                    // }
                ],
                list: [],
                open: function (item, type) {
                    var _this = $scope.teamState;
                    $scope.modalList.open('teamState', function () {
                        _this.open(item);
                    }, _this.close, type);
                    _this.init(item);
                },
                close: function () {
                    $scope.modalList.close('teamState');
                },
                init: function (item) {
                    var _this = this;
                    var _params = {
                        taskid: item.taskid,
                        helperid: item.helperid,
                        roleid: item.roleid
                    };
                    _this.loading = true;
                    $http.get(dispatchUrl.teamState, { params: _params }).success(function (res) {
                        _this.loading = false;
                        res = commonService.toJson(res);
                        if (res && res.state) {
                            var _item = {};
                            var _list = [];
                            angular.forEach(res.data.list, function (item) {
                                switch (+item.xstatus) {
                                    case 2:
                                        // 已分配
                                        _item = angular.copy(_this.data[1]);
                                        break;
                                    case 4:
                                        // 协同失败
                                        _item = angular.copy(_this.data[2]);
                                        break;
                                    default:
                                        // 已完成
                                        _item = angular.copy(_this.data[0]);
                                }
                                _item.time = (+item.time) * 1000;
                                if (_item.author) {
                                    _item.msg = res.data.helperinfo.job + res.data.helperinfo.helper + _item.msg;
                                }
                                if (_item.phone) {
                                    _item.phone = res.data.helperinfo.phone;
                                }
                                _list.push(_item);
                            });
                            _this.list = _list;
                        } else {
                            alertMsg('获取协同状态失败', res);
                        }
                    });
                }
            };
            // 回收站的操作
            $scope.recycleHandle = function (name, params) {
                var _url = '';
                var _params = {};
                // 三种类型的数据，列表、操作接口都不是一致
                switch ($scope.dispatchSelect.navbar) {
                    case 'recycle_live':
                        _params.liveid = $scope.dispatchSelect.item.id;
                        _url = dispatchUrl.live_handle;
                        switch (params) {
                            case 'reset':
                                _params.status = 6;
                                break;
                            case 'clear':
                                _params.status = 99;
                                _params.liveid = 'all';
                                break;
                            default:
                                // delete
                                _params.status = 99;
                                break;
                        }
                        break;
                    case 'recycle_clue':
                        _params.id = $scope.dispatchSelect.item.id;
                        _url = dispatchUrl.cluehandle;
                        switch (params) {
                            case 'reset':
                                _params.status = 3;
                                break;
                            case 'clear':
                                _params.status = -3;
                                delete _params.id;
                                break;
                            default:
                                // delete
                                _params.status = -2;
                                break;
                        }
                        break;
                    // recycle_task
                    default:
                        _params.taskid = $scope.dispatchSelect.item.id;
                        _url = dispatchUrl.taskhandle;
                        switch (params) {
                            case 'reset':
                                _params.status = -1;
                                break;
                            case 'clear':
                                _params = null;
                                _url = dispatchUrl.taskClear;
                                break;
                            default:
                                // delete
                                _params.status = 99;
                                break;
                        }
                }
                $scope.dispatchEdit.handle(name, _url, _params);
            };
            // 直播回顾删除
            $scope.liveListHandle = function (name, params) {
                var _url = dispatchUrl.live_handle;
                var _params = {
                    liveid: $scope.dispatchSelect.item.id,
                    status: 7
                };
                $scope.dispatchEdit.handle(name, _url, _params);
            };

            // amap look
            // 高德地图api接口key,每个接口的查询量每日/2000次,个人开发上限。
            $scope.amapMarker = {
                key: {
                    JSAPI: '1419b87be54938a559a18c3af1a4ed07',
                    WEBAPI: 'c8bbb6b513a8c7d745ce5e3ae73324d2'
                },
                tpl: '/assets/module/report/index/template/map/amap-look.html',
                pos: '',
                info: '',
                close: function () {
                    $scope.modalList.close('amapLook');
                    $timeout(function () {
                        $scope.amapMarker.url = '';
                    }, 150);
                },
                open: function (info, pos, name) {
                    var _pos = pos.split(",");
                    if (!info || !_pos[0] || !_pos[1]) {
                        return alertMsg('地址有误', { error: '地址详情或经纬度为空' });
                    }
                    var _this = $scope.amapMarker;
                    _this.info = info;
                    _this.pos = pos;
                    $scope.modalList.open('amapLook', function () {
                        _this.open(info, pos, name);
                    }, _this.close, name);
                    _this.url = _this.tpl;
                }
            };

            // modal 公用方法 修改时需注意调度中心里面很多地方都调用了
            $scope.modalList = {
                state: {},
                loading: {},
                fn: {},
                selected: {},
                currentType: '',
                keyboardBind: function (e) {
                    var _this = $scope.modalList;
                    if (e.keyCode === 27 && _this.currentType) {
                        $scope.$apply(function () {
                            _this.fn[_this.currentType].close();
                        });
                    }
                },
                init: function () {
                    document.addEventListener('keyup', $scope.modalList.keyboardBind);
                },
                open: function (type, open, close, name) {
                    var _time = 0;
                    var _this = this;
                    _this.currentType = type;
                    // 在深度为2时清除上一层的回调入口 但不能清除函数引用
                    if (_this.selected[name]) {
                        // _this.fn[_this.selected[name]] = null;
                        _this.selected[name] = '';
                    }
                    if (name && _this.fn[name]) {
                        _this.selected[type] = name;
                        _this.fn[name].close();
                        _time = 150;
                    }
                    _this.fn[type] = {
                        open: open,
                        close: close
                    };
                    $timeout(function () {
                        _this.state.common = true;
                        _this.state[type] = true;
                    }, _time);
                },
                close: function (type) {
                    if (!type) {
                        document.removeEventListener('keyup', $scope.modalList.keyboardBind);
                        return;
                    }
                    var _this = this;
                    _this.state.common = null;
                    _this.state[type] = null;
                    if (_this.selected[type]) {
                        $timeout(function () {
                            _this.fn[_this.selected[type]].open();
                            // _this.fn[_this.selected[type]] = null;
                            _this.selected[type] = '';
                        }, 150);
                    }
                    $timeout(function () {
                        $(".collect-modal").scrollTop(0);
                    }, 300);
                }
            };

            // alert 弹窗
            $scope.alertMsg = alertMsg;
            function alertMsg(msg, res) {
                (function () {
                    new Modal({
                        title: msg,
                        content: res && res.error ? res.error : ''
                    }).open();
                }());
            }

            // 中间列表相关处理
            function initList() {
                $scope.list = [];
                var list = $scope.nav.list, _list = $scope.nav._list;
                if ([8, 9, 10, 11, 12, 13].indexOf($scope.isactive) === -1) {
                    var ids = [];
                    if (list && list.length) {
                        $.each(list, function (i, item) {
                            ids.push(item.id);
                            $scope.list.push(item);
                        });
                    }
                    if (_list && _list.length) {
                        $.each(_list, function (i, item) {
                            if (ids.indexOf(item.id) < 0) {
                                $scope.list.unshift(item);
                            }
                        });
                    }
                } else {
                    // dispatch
                    // angular.forEach(list,function(item){
                    //   switch(item.from){
                    //     case 1: item.fromname = '报料';break;
                    //     case 2: item.fromname = '外部接口报料';break;
                    //     case 3: item.fromname = '电话报料';break;
                    //     default:
                    //   }
                    // });
                    $scope.list = list;
                }
            }
            function getListData(index, fn) {
                var url = 'Content/list';
                switch (index) {
                    case 8:
                    case 5:
                    case 6:
                        url = 'Content/livelist';
                        break;
                    case 9:
                        url = 'dispatch/clue';
                        break;
                    case 10:
                    case 13:
                        url = 'dispatch/task';
                        break;
                    case 11:
                    case 15:
                        url = '';
                        break;
                    case 12:
                        break;
                    case 16:
                        switch ($scope.dispatchSelect.navbar) {
                            case 'recycle_live':
                                url = 'Content/livelist';
                                break;
                            case 'recycle_clue':
                                url = 'dispatch/clue';
                                break;
                            default:
                                // task
                                url = 'dispatch/task';
                        }
                        break;
                    default:
                }
                if (url) {
                    if ($scope.dispatchSelect.keyword) {
                        navQuery[index].keyword = $scope.dispatchSelect.keyword;
                    } else {
                        delete navQuery[index].keyword;
                    }
                    if (!$scope.more) {
                        navQuery[index].offset = 0;
                    }
                    url = $scope.appbaseurl + url;
                    $http.post(url, navQuery[index])
                        .success(function (res) {
                            res = commonService.toJson(res);
                            if (res.state) {
                                navQuery[index].offset += res.data.length;
                            }
                            fn(res, index);
                        })
                        .error(function () {
                            Modal.error('请求失败');
                        });
                } else {
                    $scope.isactive = index;
                    $scope.dispatchSelect.init(index);
                    $scope.sortList && $scope.sortList.reset(index);
                }
            }

            // 过滤类型
            function filterModelid(id) {
                var str = '';
                switch(id) {
                    case 2: str = '图集';break;
                    case 3: str = '直播';break;
                }
                if(str) Modal.alert('暂不支持' + str + '类型');
                return str;
            }

            // 旧代码
            function list_merge(list, data) {
                $.each(data, function (i, item) {
                    list.push(item);
                });
            }
            function calcContentWidth() {
                var minWidth = 220;
                var w = $('.contentContainer').width() ? $('.contentContainer').width() : 800;
                var distanceCol = 20;

                return ~~((w + distanceCol) / (minWidth));
            }
            function listRemove(list, id) {
                var isDone = false;
                var retItem = null;
                $.each(list, function (i, item) {
                    if (isDone) return;
                    if (item.id == id) {
                        isDone = true;
                        retItem = list.splice(i, 1);
                    }
                });

                return retItem;
            }
            // 旧代码 end
        }
    ]);
});
